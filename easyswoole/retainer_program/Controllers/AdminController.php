<?php
use App\UserManagement\ViewModels\AdminViewModel;

class AdminController extends MasterController{

    function firstLoad(){

    }

    public function findAllReport($srv, $params)
    {
        try {
            $this->authorize('admin');
            $auth = $this->getPayload();

            $request = $srv->get('GET.request');
            $request = json_decode($request, 1);
            $allowed_request = 'email,username,full_name,group_name,status';

            $data = $this->validateSearchRequest($request, $allowed_request);

            $AdminViewModel = new AdminViewModel();

            $result = $AdminViewModel->findAllReport($data['search'], $data['order_by'], $data['current_page'], $data['limit_per_page'], null, $request['download']);

            return $this->sendResult($result);
        } catch (Exception $e) {
            return $this->sendError($e);
        }
    }

    function find($srv, $params) {
        try {
            $this->authorize("admin");
            $request = json_decode($srv->get('GET.request'), 1);

            $search = $request['search'] ?: [];

            $AdminViewModel = new AdminViewModel();

            $result = $AdminViewModel->findAdmin($search);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function changeAdminPassword($srv, $params) {
        try {
            $this->authorize("admin");

            $this->checkAcceptedRequiredBodyParams([
                'username'=>expectedResultString()->required(),
                'password'=>expectedResultString()->required(),
            ]);

            $getBody = $this->getBody();

            $AdminViewModel = new AdminViewModel();

            $result = $AdminViewModel->changeAdminPassword($getBody);
            
            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function addNewAdmin($srv, $params) {
        try {
            $this->authorize("admin");

            $getBody = $this->getBody();

            $AdminViewModel = new AdminViewModel();

            $result = $AdminViewModel->addNewAdmin($getBody);
            
            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function updateAdmin($srv, $params) {
        try {
            $this->authorize("admin");

            $auth = $this->getPayload();
            $username = $params['username'];
            $getBody = $this->getBody();

            $AdminViewModel = new AdminViewModel();

            $result = $AdminViewModel->updateAdmin($username, $getBody);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function deleteAdmin($srv, $params) {
        try {
            $this->authorize("admin");

            $auth = $this->getPayload();
            $username = $params['username'];

            $AdminViewModel = new AdminViewModel();

            $result = $AdminViewModel->deleteAdmin($username);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

}