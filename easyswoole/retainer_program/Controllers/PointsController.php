<?php
use App\RetailerProgram\ViewModels\PointsViewModel;

class PointsController extends MasterController{

    function firstLoad(){

    }

    public function findAllReport($srv, $params)
    {
        try {
            $this->authorize('admin');

            $request = $srv->get('GET.request');
            $request = json_decode($request, 1);
            $allowed_request = 'username,full_name,type,cell_phone,email,dob,gender,address,membership,points,created_date,user,merchant_name,merchant_code,city,contact_person,cell_phone,work_phone';

            $data = $this->validateSearchRequest($request, $allowed_request);

            $PointsViewModel = new PointsViewModel();

            $result = $PointsViewModel->findAllReport($data['search'], $data['order_by'], $data['current_page'], $data['limit_per_page'], null, $request['download']);

            return $this->sendResult($result);
        } catch (Exception $e) {
            return $this->sendError($e);
        }
    }

    function find($srv, $params) {
        try {
            $this->authorize("admin");
            $request = json_decode($srv->get('GET.request'), 1);
            
            $search = $request['search'] ?: [];

            $PointsViewModel = new PointsViewModel();

            $result = $PointsViewModel->find($search);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function getUserPointsBalance($srv, $params) {
        try {
            $this->authorize("member");
            
            $auth = $this->getPayload();

            $PointsViewModel = new PointsViewModel();
            $result = $PointsViewModel->getUserPointsBalance($auth);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }
}