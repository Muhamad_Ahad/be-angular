<?php
use App\GeneralData\ViewModels\AdministrativeRegionViewModel;
use App\GeneralData\ViewModels\CourierViewModel;

class AdministrativeRegionController extends MasterController{

    function firstLoad(){

    }

    function find($srv, $params) {
        try {
            $this->authorize("admin");

            $request = json_decode($srv->get('GET.request'), 1);

            $search = $request['search'] ?: [];

            $AdministrativeRegionViewModel = new AdministrativeRegionViewModel();

            $result = $AdministrativeRegionViewModel->findAdminGroup($search);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function getSAPServiceList($srv, $params) {
        try {

            $CourierViewModel = new CourierViewModel();

            $result = $CourierViewModel->getSAPRegion();

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function searchRegion($srv, $params) {
        try {
            $this->authorize("admin");

            $this->checkAcceptedRequiredBodyParams(
                [
                    'region'=>expectedResultString()->required(),
                    'search'=>expectedResultString()->required(),
                    'province_code'=>expectedResultString(),
                    'city_code'=>expectedResultString(),
                    'subdistrict_code'=>expectedResultString()
                ]
            );

            $getBody = $this->getBody();
            
            $AdministrativeRegionViewModel = new AdministrativeRegionViewModel();

            $result = $AdministrativeRegionViewModel->searchRegion($getBody);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }

    function updateRegions($srv, $params) {
        try {
            $this->authorize("admin");

            /* $file = $srv->get("FILES");
            if (empty($file['document'])) {
                throw new Exception("upload document is required", 400);
            } */

            $auth = $this->getPayload();
            $AdministrativeRegionViewModel = new AdministrativeRegionViewModel();

            $result = $AdministrativeRegionViewModel->updateRegions($auth, $file['document']);

            return $this->sendResult($result);
        } catch (Throwable $e){
            return $this->sendError($e);
        }
    }
}