<?php
namespace App\UserManagement\ViewModels\AppListViewModel;

use App\UserManagement\Models\AppListModel;
use App\UserManagement\ViewModels\AdminViewModel;

function addNewAppList($arguments, $thisViewModel) {
    $bodyData = $arguments[0];
    try {
        $AppListModel = new AppListModel();

        $bodyData['label'] = trim($bodyData['label']);
        unset($bodyData['status']);
        $find = $AppListModel->findByAppLabel($bodyData['label']);
        if (!empty($find['result'])) {
            $thisViewModel->sendError("app label already exists", 409);
        }

        $bodyData['status'] = 'ACTIVE';

        $result = $AppListModel->insert($bodyData);

        return $result;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}