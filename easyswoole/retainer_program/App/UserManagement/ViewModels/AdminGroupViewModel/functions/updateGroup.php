<?php
namespace App\UserManagement\ViewModels\AdminGroupViewModel;

use App\UserManagement\Models\AdminGroupModel;
use App\UserManagement\Models\AccessListModel;
use App\UserManagement\Models\AppListModel;

function updateGroup($arguments, $thisViewModel) {
    $getParams = $arguments[0];
    $bodyData = $arguments[1];
    try {
        $acceptedBodyData = 'description,status,access_list';

        $AdminGroupModel = new AdminGroupModel();
        $find = $AdminGroupModel->findByGroupName($getParams);

        if (empty($find['result'])) {
            $thisViewModel->sendError("admin group does not exists", 404);
        }

        $acceptedField = explode(",", $acceptedBodyData);
        foreach ($bodyData as $key => $value) {
            if(!in_array($key, $acceptedField)) {
                unset($bodyData[$key]);
            }
        }

        if (!empty($bodyData['access_list'])) {
            $AccessListModel = new AccessListModel();
            $AppListModel = new AppListModel();

            foreach ($bodyData['access_list'] as $key => $value) {
                $findApp = $AppListModel->findByAppLabel($key, 'ACTIVE');
                if (empty($findApp['result'])){
                    $thisViewModel->sendError("App label is invalid", 400);
                }

                if (empty($value) || !is_array($value)) {
                    $thisViewModel->sendError("access menu list be an array", 400);
                }

                $filter = [
                    'name'=>$value,
                    'app_label'=>$key,
                    'status'=>'ACTIVE'
                ];
                $findAccess = $AccessListModel->getAccessNameList($filter);
                if (empty($findAccess['result'])) {
                    $thisViewModel->sendError("Access menu does not exists", 404);
                }
                foreach ($value as $sk => $sval) {
                    if (!in_array($sval, $findAccess['result'][0]->name)) {
                        $thisViewModel->sendError("access {$sval} is invalid", 400);
                    }
                }
                $bodyData['access_list'][$key] = implode("|", $value);
            }
        }

        $result = $AdminGroupModel->update(
            ['_id'=>$find['result'][0]->_id],
            $bodyData
        );

        return $result;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}