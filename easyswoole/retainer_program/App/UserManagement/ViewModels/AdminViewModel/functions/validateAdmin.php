<?php
namespace App\UserManagement\ViewModels\AdminViewModel;

use App\UserManagement\Models\AdminModel;
use App\GeneralData\ViewModels\CryptoViewModel;

function validateAdmin($arguments, $thisViewModel) {
    $username = $arguments[0];
    $password = $arguments[1];

    try {

        $AdminModel = new AdminModel();
        $result = $AdminModel->findByUsernameOrEmail(['username'=>$username, 'status'=>'ACTIVE']);

        if (empty($result['result'])) {
            $thisViewModel->sendError("admin {$username} is not valid", 400);
        }

        $hash = $result['result'][0]->password;

        $CryptoViewModel = new CryptoViewModel();
        $verify = $CryptoViewModel->verifyPassword($password, $hash);
        if (!$verify) {
            $thisViewModel->sendError("admin password is not valid", 400);
        }

        return $thisViewModel->objectToArray($result['result'][0]);
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}