<?php
namespace App\UserManagement\ViewModels\AdminViewModel;

use App\UserManagement\Models\AdminModel;
use App\UserManagement\Models\AdminGroupModel;
use App\GeneralData\ViewModels\CryptoViewModel;

function addNewAdmin($arguments, $thisViewModel) {
    $bodyData = $arguments[0];
    try {
        $emptyField = [];        
        empty($bodyData['email'])?
            $emptyField[] = "email":
            null;

        empty($bodyData['full_name'])?
            $emptyField[] = "full_name":
            null;
        
        empty($bodyData['username'])?
            $emptyField[] = "username":
            null;
        
        empty($bodyData['password'])?
            $emptyField[] = "password":
            null;

        if (!empty($emptyField)) {
            $fieldName = implode(", ", $emptyField);
            $thisViewModel->sendError("field ".$fieldName." is required", 400);
        }

        $AdminModel = new AdminModel();

        $bodyData['username'] = trim($bodyData['username']);
        $bodyData['email'] = trim($bodyData['email']);
        unset($bodyData['status']);
        $find = $AdminModel->findByUsernameOrEmail($bodyData);

        if (!empty($find['result'])) {
            $thisViewModel->sendError("Admin username or email already exists", 409);
        }

        if (!empty($bodyData['group_name'])) {
            $AdminGroupModel = new AdminGroupModel();

            $group = $AdminGroupModel->findByGroupName($bodyData['group_name'], 'ACTIVE');

            if (empty($group['result'])){
                $thisViewModel->sendError("admin group {$bodyData['group_name']} does not exists", 404);
            }

            $bodyData['group_id'] = $group['result'][0]->_id;
        }

        $CryptoViewModel = new CryptoViewModel();

        $bodyData['password'] = $CryptoViewModel->generatePassword($bodyData['password'], 14);
        $bodyData['status'] = 'ACTIVE';

        $result = $AdminModel->insert($bodyData);

        return $result;
    } catch (\Exception $e) {
        return $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        return $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}