<?php
namespace App\UserManagement\ViewModels\AuthViewModel;

use App\UserManagement\Models\AdminAuthorizationModel;
use App\UserManagement\ViewModels\AdminViewModel;
use App\UserManagement\ViewModels\AdminGroupViewModel;
use App\UserManagement\ViewModels\AppListViewModel;
use App\UserManagement\ViewModels\AccessListViewModel;

function authorizeAdmin ($arguments, $thisViewModel){
    $token = $arguments[0];
    $authPayload = $arguments[1];
    $access = $arguments[2];
    try {

        // validate admin auth token
        $AdminAuthorizationModel = new AdminAuthorizationModel();
        $filter = [
            'token' => $token,
            'active'=> 1
        ];
        $find = $AdminAuthorizationModel->find($filter);
        if (empty($find['result'])) {
            $thisViewModel->sendError("Unauthorized, invalid authorization token", 401);
        }


        // validate admin
        $AdminViewModel = new AdminViewModel();
        $search = [
            '_id'=>$authPayload['subject_id'],
            'status'=>['eq'=>'ACTIVE']
        ];
        $findAdmin = $AdminViewModel->findAdmin($search);
        if (empty($findAdmin['result'])) {
            $thisViewModel->sendError("Unauthorized, permission invalid", 401);
        }


        // validate admin group
        $AdminGroupViewModel = new AdminGroupViewModel();
        $filter = [
            '_id'=>$findAdmin['result'][0]->group_id,
            'status'=>['eq'=>'ACTIVE']
        ];
        $findGroup = $AdminGroupViewModel->findAdminGroup($filter);
        if (empty($findGroup['result'])) {
            $thisViewModel->sendError("Unauthorized, invalid admin group", 401);
        }
        $findGroup = $thisViewModel->objectToArray($findGroup['result'][0]);


        // validate application
        $AppListViewModel = new AppListViewModel();
        $filter = [
            'app_label'=>['eq'=>$access['app_label']],
            'status'=>['eq'=>'ACTIVE']
        ];
        $findApp = $AppListViewModel->findApp($filter);
        if (empty($findApp['result'])) {
            $thisViewModel->sendError("Unauthorized, invalid access", 403);
        }

        // validate access
        $AccessListViewModel = new AccessListViewModel();
        $search = [
            'endpoint'=>['eq'=>$access['endpoint']],
            'app_label'=>['eq'=>$access['app_label']],
            'status'=>['eq'=>'ACTIVE']
        ];
        $findAccess = $AccessListViewModel->findAccessList($search);
        
        if (!empty($findAccess['result'])) {
            $accName = $findAccess['result'][0]->name;
            if (empty($findGroup['access_list'][$access['app_label']])) {
                $thisViewModel->sendError("Unauthorized, access denied", 403);
            }
            $accList = explode("|", $findGroup['access_list'][$access['app_label']]);
            if (!in_array($accName, $accList)) {
                $thisViewModel->sendError("Unauthorized, access not permitted", 403);
            }
        }

    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}