<?php
namespace App\UserManagement\ViewModels\AuthViewModel;

use App\RetailerProgram\ViewModels\MemberViewModel;
use App\RetailerProgram\Models\MemberAuthorizationModel;

function authorizeMember ($arguments, $thisViewModel){
    $token = $arguments[0];
    $authPayload = $arguments[1];
    $access = $arguments[2];
    try {

        // validate member auth token
        $MemberAuthorizationModel = new MemberAuthorizationModel();
        $filter = [
            'token' => $token,
            'active'=> 1
        ];
        $find = $MemberAuthorizationModel->find($filter);
        if (empty($find['result'])) {
            $thisViewModel->sendError("Unauthorized, invalid authorization token", 401);
        }


        // validate member
        $MemberViewModel = new MemberViewModel();
        $search = [
            '_id'=>$authPayload['subject_id'],
            'status'=>['eq'=>'ACTIVE']
        ];
        $findMember = $MemberViewModel->findMember($search);
        if (empty($findMember['result'])) {
            $thisViewModel->sendError("Unauthorized, permission invalid", 401);
        }
        
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}