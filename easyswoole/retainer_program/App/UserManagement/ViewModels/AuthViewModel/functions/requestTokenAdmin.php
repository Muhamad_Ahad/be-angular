<?php
namespace App\UserManagement\ViewModels\AuthViewModel;

use App\UserManagement\Models\AdminAuthorizationModel;
use App\UserManagement\ViewModels\AdminViewModel;
use App\GeneralData\ViewModels\CryptoViewModel;

function requestTokenAdmin ($arguments, $thisViewModel){
    $bodyData = $arguments[0];
    try {
        $AdminViewModel = new AdminViewModel();
        $adminData = $AdminViewModel->validateAdmin($bodyData['username'], $bodyData['password']);

        $iat = time();
        $exp = 36000;

        $auth = [
            'type' => 'admin',
            'user_id'=>$adminData['_id'],
            'subject_id'=>$adminData['_id'],
            'admin_username'=>$adminData['username'],
            'full_name'=>$adminData['full_name'],
            'issued_time'=>$iat,
            'expiry_time'=>$exp,
            'active'=>1
        ];

        $CryptoViewModel = new CryptoViewModel();
        $token = $CryptoViewModel->generateJWT($auth);

        $auth = array_merge(
            ['token'=>$token],
            $auth
        );

        $AdminAuthorizationModel = new AdminAuthorizationModel();
        $saveToken = $AdminAuthorizationModel->insert($auth);

        $result = [
            'result'=>[
                'token'=>$token,
                'type' => 'admin',
                'admin_username'=>$adminData['username'],
                'full_name'=>$adminData['full_name'],
                'issued_time'=>$iat,
                'expiry_time'=>$exp,
                'active'=>1
            ]
        ];

        return $result;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}