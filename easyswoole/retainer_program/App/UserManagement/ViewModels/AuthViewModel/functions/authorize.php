<?php
namespace App\UserManagement\ViewModels\AuthViewModel;

use App\UserManagement\ViewModels\AppListViewModel;
use App\GeneralData\ViewModels\CryptoViewModel;

function authorize ($arguments, $thisViewModel){
    $token = $arguments[0];
    $type = $arguments[1];
    $access =  $arguments[2];
    try {

        !is_array($type) ? $type = array($type) : null;

        $CryptoViewModel = new CryptoViewModel();
        $authPayload = $CryptoViewModel->validateJWT($token);

        $expTime = $authPayload['issued_time'] + $authPayload['expiry_time'];
        if (time() > $expTime)
            $thisViewModel->sendError("Unauthorized, authorization token expired", 401);

        if (!in_array($authPayload['type'], $type)) {
            $thisViewModel->sendError("Unauthorized, permission denied", 401);
        }

        // validate application
        $AppListViewModel = new AppListViewModel();
        $appName = $AppListViewModel->validateApp($access['app_label']);

        switch ($authPayload['type']) {
            case 'admin':
                $result = $thisViewModel->authorizeAdmin($token, $authPayload, $access);
                break;
            
            case 'member':
                $result = $thisViewModel->authorizeMember($token, $authPayload);
                break;
        }

        $authPayload['app_name'] = $appName['app_name'];
        $authPayload['app_prefix'] = $appName['app_prefix'];

        return $authPayload;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}