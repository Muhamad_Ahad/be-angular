<?php
namespace App\UserManagement\Models;

use Infrastructure\MongoModel;

class AdminGroupModel extends MongoModel {
    public $table;
    public $dbName = "user_management";
    public $collectionName = "dtm_admin_group";

    public $requestColumns = "_id,group_name,description,status,access_list,count";
    public $requestMapping = [
        '_id' => '$_id',
        'group_name'=>'$group_name',
        'description'=>'$description',
        'status'=>'$status',
        'access_list'=>'$access_list'
    ];

    function find($filter) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            $match = [];
            foreach ($filter as $key => $column) {
                if (isset($this->requestMapping[$key])) {
                    $request_mapping = substr($this->requestMapping[$key], 1);
                    if ($key == '_id') {
                        $match[$request_mapping] = $this->convertToObjectId($column);
                    } else {
                        $match[$request_mapping] = $this->convertValueToMongoQuery($column);
                    }
                }
            }

            return $this->DBfind($match);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function findByGroupName($groupName, $status = null) {
        try {
        
            $filter = ['group_name'=>$groupName];

            !empty($status)?
                $filter['status'] = $status:
                null;

            return $this->DBfind($filter);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function insert($bodyData) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            return $this->DBinsert($bodyData);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function update($filter, $bodyData) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            if (!empty($filter['_id'])) {
                $filter['_id'] = $this->convertToObjectId($filter['_id']);
            }
            return $this->DBupdate($filter, $bodyData);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }
}