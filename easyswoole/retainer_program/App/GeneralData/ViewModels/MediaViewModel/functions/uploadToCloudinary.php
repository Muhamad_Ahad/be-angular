<?php
namespace App\GeneralData\ViewModels\MediaViewModel;

use Services\CloudinaryClient;

function uploadToCloudinary ($arguments, $thisViewModel){
    $uploadType = $arguments[0];
    $fileInfo = $arguments[1];
    $cloudinaryConfig = $arguments[2];
    $uploadConfig = $arguments[3];
    $request = $arguments[4];

    $useDefaultPreset = true;
    try{
        $cloudinaryClient = CloudinaryClient::getInstance(
            [
                'cloud_name' => $cloudinaryConfig->cloud_name,
                'api_key' => $cloudinaryConfig->api_key,
                'api_secret' => $cloudinaryConfig->api_secret,
                'secure' => true,
                'sign_url' => true,
            ]
        );

        $insertData = [];
        if ($uploadType == 'image') {
            foreach ($fileInfo as $key => $value) {
                $options = [
                    'folder'=>$value['folder'],
                    'public_id'=>$value['filename'],
                    'tags'=>$value['image_tag']
                ];
                $transformation = [];
                foreach ($uploadConfig['image_transform'] as $sk => $sval) {
                    $uploadOptions = [];
                    if($sval['width'] != 'auto') $uploadOptions['width'] = $sval['width'];
                    if($sval['height'] != 'auto') $uploadOptions['height'] = $sval['height'];
                    if(isset($sval['mode'])) $uploadOptions['crop'] = $sval['mode'];
                    if(isset($sval['quality'])) $uploadOptions['quality'] = $sval['quality'];

                    if (!empty($uploadOptions)) {
                        $transformation[] = $sval['name'];
                        $options['eager'][] = $uploadOptions;
                    }
                }
                $result = $cloudinaryClient->uploadImage($value['file_path'], $options, $useDefaultPreset);

                $webUrl = (isset($result['secure_url']))? $result['secure_url'] : $result['url'];
                if (isset($result['eager'])) {
                    $eagerResponse = [];
                    if(count($result['eager']) == 1){
                        $svalue = $result['eager'][0];
                        $eagerResponse = (isset($svalue['secure_url']))? $svalue['secure_url'] : $svalue['url'];
                    } else {
                        foreach ($result['eager'] as $skey => $svalue) {
                            $eagerResponse[] = (isset($svalue['secure_url']))? $svalue['secure_url'] : $svalue['url'];
                        }
                    }
                }

                $setData = [
                    'upload_type' => $uploadType,
                    'file_type' => $value['type'],
                    'file_name' => $value['filename'],
                    'file_format' => $result['format'],
                    'public_id' => $result['public_id'],
                    'web_url' => $webUrl,
                    'upload_response'=> $result
                ];
                if(!empty($eagerResponse)){
                    $setData['eager_response'] = $eagerResponse;
                }

                $publicId = $setData['public_id'].'.'.$setData['file_format'];
                $uploadResult = [];
                $uploadResult['base_url'] = "https://res.cloudinary.com/".$cloudinaryConfig->cloud_name."/".$result['resource_type']."/".$result['type']."/";
                
                $uploadResult['pic_big_path'] = substr($webUrl, strlen($uploadResult['base_url']));
                $uploadResult['pic_file_name'] = substr($webUrl, strlen($uploadResult['base_url']));

                foreach ($transformation as $k => $val) {
                    $uploadResult[$val] = substr($eagerResponse[$k], strlen($uploadResult['base_url']));
                }

                /* if(!empty($request)){
                    $uploadOptions = [];
                    if($request['width']) $uploadOptions['resize']['width'] = (int)$request['width'];
                    if($request['height']) $uploadOptions['resize']['height'] = (int)$request['height'];
                    if($request['width'] || $request['height']) $uploadOptions['resize']['mode'] = 'limitFit';
                    $uploadOptions['quality'] = 'autoGood';
                    // $uploadOptions['secure'] = true;
                    // $uploadOptions['sign_url'] = true;
                    $transformedImage = $cloudinaryClient->signedImageTransformation($publicId, $uploadOptions);
                    $uploadResult['request_pic_path'] = substr($transformedImage, strlen($uploadResult['base_url']));
                } */

                $setData['image_url'] = $uploadResult;
                $uploadData[$key] = $setData;
            }
        } elseif ($uploadType == 'document') {
            $options = [
                'folder'=>$fileInfo['folder'],
                'public_id'=>$fileInfo['filename'],
                'resource_type'=>'raw'
            ];

            if ($fileInfo['type'] == 'application/pdf') {
                $options['resource_type'] = 'image';
            }
            $uploadData = $cloudinaryClient->uploadDocument($fileInfo['file_path'], $options);
        }
        return $uploadData;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}