<?php
namespace App\GeneralData\ViewModels\AdministrativeRegionViewModel;

use App\GeneralData\Models\AdministrativeRegionModel;

function updateRegions($arguments, $thisViewModel) {
    $auth = $arguments[0];
    try {

        $jsonString = @file_get_contents(PROJECT_PATH."assets/wilayah_2020.json");
        $regionData = json_decode($jsonString, 1);

        $regExr = "/((?<=\()([a-zA-Z0-9\s\-\'\.]+))|((?<=\))([a-zA-Z0-9\s\-\'\.]+))|((?<=\/)([a-zA-Z0-9\s\-\'\.]+))|(([a-zA-Z0-9\s\-\'\.]+)(?=\/))|([a-zA-Z0-9\s\-\'\.]+)/";
        $province=[];
        $city = [];
        $subdistrict = [];
        $village = [];

        $newRegions = [];
        $region = [];
        foreach ($regionData as $row => $data) {
            $data['nama'] = strtoupper(trim($data['nama']));
            preg_match_all($regExr, $data['nama'], $name);
            
            $regionName = [];
            if (count($name[0]) > 1) $regionName[] = $data['nama'];
            foreach ($name[0] as $key => $value) {
                $value = trim($value);
                if(empty($value) || strlen($value) <= 3) continue;

                $regionName[] = $value;
            }

            switch (strlen($data['kode'])) {
                case 2:
                    $province[$data['kode']] = $regionName;
                    $region['province'] = $regionName;
                    $region['province_code'] = $data['kode'];
                    break;
                
                case 5:
                    if(strpos($data['nama'], 'KAB') !== false || strpos($data['nama'], 'KOTA') !== false) {
                        $regionName[0] = trim(substr($data['nama'], 4));
                        $region['regional_level2'] = substr($data['nama'], 0, 4);
                    }
                    $city[$data['kode']] = $regionName;
                    $region['city'] = $regionName;
                    $region['city_code'] = $data['kode'];
                    break;
                
                case 8:
                    $subdistrict[$data['kode']] = $regionName;
                    $region['subdistrict'] = $regionName;
                    $region['subdistrict_code'] = $data['kode'];
                    break;

                case 13:
                    $village[$data['kode']] = $regionName;
                    $region['village'] = $regionName;
                    $region['village_code'] = $data['kode'];
                    $newRegions[$data['kode']] = $region;
                    break;
            }
        }

        $AdministrativeRegionModel = new AdministrativeRegionModel();
        $AdministrativeRegionModel->setCurrentUser($auth['user_id']);
        $admRegions = $AdministrativeRegionModel->find([]);
        $admRegions = $thisViewModel->objectToArray($admRegions['result']);

        $updateRegion = [];
        foreach ($admRegions as $row => $data) {
            if (empty($newRegions[$data['village_code']])) {
                $updateData['deleted'] = 1;
                continue;
            }
            $updateData = [];

            foreach ($city[$data['city_code']] as $key => $value) {
                if (!in_array($value, $data['city'])) {
                    $data['city'][] = $value;
                    $updateData['city'] = $data['city'];
                }
            }

            foreach ($subdistrict[$data['subdistrict_code']] as $key => $value) {
                if (!in_array($value, $data['subdistrict'])) {
                    $data['subdistrict'][] = $value;
                    $updateData['subdistrict'] = $data['subdistrict'];
                }
            }

            foreach ($village[$data['village_code']] as $key => $value) {
                if (!in_array($value, $data['village'])) {
                    $data['village'][] = $value;
                    $updateData['village'] = $data['village'];
                }
            }

            if (!empty($updateData)) {
                $updateRegion[] = [
                    'filter'=>['_id'=>$AdministrativeRegionModel->convertToObjectId($data['_id'])],
                    'new_value'=>$updateData
                ];
            }
            unset($newRegions[$data['village_code']]);
        }

        !empty($updateRegion) ?
            $AdministrativeRegionModel->updateBatch($updateRegion) : null;

        !empty($newRegions) ?
            $AdministrativeRegionModel->insertBatch($newRegions) : null;
        
        return ['result'=>$newRegions];
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}