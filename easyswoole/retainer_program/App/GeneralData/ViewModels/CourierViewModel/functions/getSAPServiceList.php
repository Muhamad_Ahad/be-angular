<?php 
namespace App\GeneralData\ViewModels\CourierViewModel;

use Services\Curl;
function getSAPServiceList($arguments, $thisViewModel) 
{
    $getBody = $arguments[0];
    try {
        $Curl = new Curl();
        $param = $thisViewModel->getCourier("REALS-SAP");

        $param['url'] = $param['api_url']['service_list'];
        $param['method'] = 'GET';
        $param['headers'] = [
                            "api-key: ".$param['api_key']['tracking'],
                            'Content-Type: application/json'
                            ];
        
        $result = $Curl->sendRequest($param);

        $dataLog = ['request_type'=>'SAP_GET_SERVICE_LIST',
                    'request_header'=>$param['headers'],
                    'request_body'=>[],
                    'request_url'=>$param['url'],
                    'response'=>$result,
                    'response_code'=>$result['response_code'],
                    'request_date'=>date('y-m-d h:i:s')
                    ];

        write_log($dataLog, 'COURIER_LOG/SAP');

        return $result['result'];
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}