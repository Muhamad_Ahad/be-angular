<?php
namespace App\GeneralData\ViewModels\CryptoViewModel;

use Services\Aes;

function generateJWT ($arguments, $thisViewModel){
    $authPayload = $arguments[0];
    $hashKey = $arguments[1] ?: $thisViewModel->jwtKey;

    try {

        $Aes = new Aes();

        $CURRENT_HASH = explode(":", $hashKey);
       
        $key1   = $CURRENT_HASH[0];
        $key2   = $CURRENT_HASH[1];
    
        $Aes->run($key1, 'CBC', $key2);
        $jwToken  = $Aes->generateJWT($authPayload);

        return $jwToken;

    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}