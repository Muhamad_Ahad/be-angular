<?php
namespace App\RetailerProgram\ViewModels\ShoppingCartViewModel;

use App\RetailerProgram\Models\ShoppingcartModel;

function addProductToShoppingCart($arguments, $thisViewModel)
{
    $auth = $arguments[0];
    $bodyData = $arguments[1];
    try {
        $ShoppingcartModel = new ShoppingcartModel();
        $ShoppingcartModel->setCurrentUser($auth['user_id']);
        
        $activeSC = $ShoppingcartModel->findActiveCartByUserID($auth['subject_id']);
        if (empty($activeSC['result'])) {
            $createNewSC = $ShoppingcartModel->createNewShoppingCartByUserID($auth['subject_id']);
            $activeSC = $ShoppingcartModel->findActiveCartByUserID($auth['subject_id']);
        }
        $activeSC = $thisViewModel->objectToArray($activeSC['result'][0]);
        
        $productData = $thisViewModel->validateProducts($bodyData['products']);

        /** Check Shopping Cart Products in Server  */ 
        $productListFromServer = [];
        foreach ($activeSC['products'] as $key => $productServer) {
            
            if (!isset($productListFromServer[$productServer['product_id']])) {
                $productListFromServer[$productServer['product_id']] = $productServer ;
            }
                
            $productListFromServer[$productServer['product_id']]['quantity']           = $productServer['quantity'];
            $productListFromServer[$productServer['product_id']]['total_product_price']= $productServer['quantity'] * $productServer['sku_value'];
        }
    
        /** Compare Product in Client and Server if different from client and server, 
         *  fix and match data in server and client
         */
        $totalQty = 0;
        $totalPrice = 0;
        foreach ($productData['products'] as $key => $newProduct) {

            $newProduct['quantity'] += $productListFromServer[$newProduct['product_id']]['quantity']?:0;
            $newProduct['total_product_price'] = $newProduct['quantity'] * $newProduct['sku_value'];

            $productListFromServer[$newProduct['product_id']] = $newProduct;

            $totalQty += $newProduct['quantity'];
            $totalPrice += $newProduct['total_product_price'];
        }
        
        $validateProduct = $thisViewModel->validateProducts($productListFromServer);
        
        $updateSC = $ShoppingcartModel->DBupdate(['_id'=>$ShoppingcartModel->convertToObjectId($activeSC['_id'])], $validateProduct);
        $result = $ShoppingcartModel->findActiveCartByUserID($auth['subject_id']);
        return $result;

    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
