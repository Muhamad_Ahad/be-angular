<?php
namespace App\RetailerProgram\ViewModels\ShoppingCartViewModel;

use App\RetailerProgram\Models\ShoppingcartModel;

function compareProduct($arguments, $thisViewModel)
{
    try {
    $currentSC = $arguments[0];
    $userID  = $arguments[1];

    $validateProduct = $thisViewModel->validateProducts($currentSC['products']);

    $ShoppingcartModel = new ShoppingcartModel();
    
    $updateSC = $ShoppingcartModel->DBupdate(['_id'=>$ShoppingcartModel->convertToObjectId($currentSC['_id'])], $validateProduct);
    $result = $ShoppingcartModel->findActiveCartByUserID($userID);
    return $result;
    
    }
    catch (\Exception $e){
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
    catch(\Error $e){
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}