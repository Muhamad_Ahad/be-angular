<?php

namespace App\RetailerProgram\ViewModels\ShoppingCartViewModel;

use App\RetailerProgram\Models\ShoppingcartModel;

function processOrderFromShoppingcart($arguments, $thisViewModel) {

    $scId = $arguments[0];
    $invoiceData = $arguments[1];

    try {
        $ShoppingcartModel = new ShoppingcartModel();

        $updateData = [
            'status'=>'INACTIVE',
            'order_id'=>$invoiceData['order_id'],
            'address_detail'=>$invoiceData['address_detail']
        ];

        $updateSC = $ShoppingcartModel->updateByID($scId, $updateData);
        
        return $updateSC;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}