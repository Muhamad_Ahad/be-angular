<?php
namespace App\RetailerProgram\ViewModels\MemberViewModel;

use App\RetailerProgram\Models\MemberModel;

function updateByInputForm($arguments, $thisViewModel) {
    $auth = $arguments[0];
    $bodyData = $arguments[1];

    try {
        $excludeField = '_id,username,password';
        $excludeField = explode($excludeField, ",");
        
        foreach ($bodyData as $key => $value) {
            if (in_array($key, $excludeField)) {
                unset($bodyData[$key]);
            }
        }
        
        $formData = $thisViewModel->validateInputForm($bodyData);
        
        $MemberModel = new MemberModel();

        $getMember = $MemberModel->findByUsernameOrId($formData['username'], "ACTIVE");
        if (empty($getMember['result'])) {
            $thisViewModel->sendError("Member does not exists", 404);
        }
        $getMember = $thisViewModel->objectToArray($getMember['result'][0]);
        $memberId = $getMember['_id'];

        $memberAddress = [
            '_id'=>$MemberModel->convertToObjectId(null, true)->__toString(),
            'name'=>$formData['receiver_name'],
            'cell_phone'=>$formData['receiver_cell_phone'],
            'address'=>$formData['address'],
            'province_name'=>$formData['province'],
            'city_name'=>$formData['city'],
            'district_name'=>$formData['district'],
            'village_name'=>$formData['village'],
            'postal_code'=>$formData['postal_code'],
            'use_as'=>'main_address'
        ];

        $addressList = [];
        if (!empty($getMember['member_address'])) {
            foreach ($getMember['member_address'] as $sk => $sval) {
                if ((!empty($memberAddress['_id'])) && $sval['id'] == $memberAddress['_id']) {
                    continue;
                }
                $sval['use_as'] = 'secondary_address';
                $addressList[] = $sval;
            }
        }
        $addressList[] = $memberAddress;
        
        $updateData = [
            // 'username'=>$formData['id_pel'],
            'full_name'=>$formData['full_name'],
            'cell_phone'=>$formData['cell_phone'],
            'member_address'=>$addressList,
            'input_form_data'=>$formData['form_input'],
            // 'additional_data'=>$bodyData['additional_data']
        ];

        if (!empty($bodyData['status']) && in_array($bodyData['status'], ['ACTIVE', 'INACTIVE'])) {
            $updateData['status'] = $bodyData['status'];
        }

        $MemberModel->setCurrentUser($auth['user_id']);
        $result = $MemberModel->updateByID($memberId, $updateData);
        return $result;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}