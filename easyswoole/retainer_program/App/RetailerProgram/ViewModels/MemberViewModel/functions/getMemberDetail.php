<?php
namespace App\RetailerProgram\ViewModels\MemberViewModel;

use App\RetailerProgram\Models\MemberModel;

function getMemberDetail($arguments, $thisViewModel) {
    $auth = $arguments[0];
    try {

        $MemberModel = new MemberModel();

        $getMember = $MemberModel->findByUsernameOrId($auth['subject_id']);
        $getMember = $thisViewModel->objectToArray($getMember['result'][0]);

        $result = [
            'username'=>$getMember['username'],
            'full_name'=>$getMember['full_name'],
            'cell_phone'=>$getMember['cell_phone'],
            'member_address'=>$getMember['member_address'],
            // 'points'=>$getMember['points']
        ];
        
        return $result;

    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}