<?php
namespace App\RetailerProgram\ViewModels\MemberViewModel;

use App\GeneralData\Models\ConfigurationModel;

function validateInputForm($arguments, $thisViewModel) {
    $bodyData = $arguments[0];
    try {

        $ConfigurationModel = new ConfigurationModel();
        $getConfig = $ConfigurationModel->findByKeyName('member-input-form')['result'][0];
        $formFields = $thisViewModel->objectToArray($getConfig->values->input_form);
        
        $result = [];

        foreach ($formFields as $key => $value) {
            if (!isset($bodyData[$key])) continue;
            
            $fieldValue = trim($bodyData[$key]);
            if (empty($fieldValue) && !empty($value['alias'])) {
                foreach ($value['alias'] as $k => $val) {
                    if (!empty($bodyData[$val])) {
                        $fieldValue = trim($bodyData[$val]);
                        break;
                    }
                }
            }
            if ($value['required'] && (empty($fieldValue) || $fieldValue == "-")) {
                $thisViewModel->sendError("field {$key} is required", 400);
            }
            $result[$key] = $fieldValue?:'';
        }

        $fieldName = $thisViewModel->objectToArray($getConfig->values->field_name);

        $returnResult = [];
        foreach ($fieldName as $key => $value) {
            foreach ($value as $k => $val) {
                if (isset($result[$val])) {
                    $data = trim($result[$val]," -\t\n\r\0\x0B");
                    $returnResult[$key] = $data;
                    if (!empty($data)) {
                        break;
                    }
                }
            }
        }
        $returnResult['form_input'] = $result;

        return $returnResult;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}