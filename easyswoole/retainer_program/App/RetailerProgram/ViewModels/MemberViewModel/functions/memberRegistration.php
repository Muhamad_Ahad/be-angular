<?php
namespace App\RetailerProgram\ViewModels\MemberViewModel;

use App\RetailerProgram\Models\MemberModel;
use App\GeneralData\ViewModels\CryptoViewModel;

function memberRegistration($arguments, $thisViewModel) {
    $auth = $arguments[0];
    $bodyData = $arguments[1];

    try {
        $formData = $thisViewModel->validateInputForm($bodyData);
        $validBody = $thisViewModel->validateBodyRegister($formData);

        $MemberModel = new MemberModel();
        $MemberModel->setCurrentUser($auth['user_id']);

        $findMember = $MemberModel->findByUsernameOrId($validBody['username']);
        if (!empty($findMember['result'])) {
            $thisViewModel->sendError("Member {$validBody['username']} already exists", 400);
        }

        /* $validBody['points']    = [
            'point_balance'=> 0 ,
            'expire_date'  => date('Y-m-d H:i:s'),
            'exp_date'     => time()
        ]; */

        $memberAddress = [
            '_id'=>$MemberModel->convertToObjectId(null, true)->__toString(),
            'name'=>$formData['receiver_name'],
            'cell_phone'=>$formData['receiver_cell_phone'],
            'address'=>$formData['address'],
            'province_name'=>$formData['province'],
            'city_name'=>$formData['city'],
            'district_name'=>$formData['district'],
            'village_name'=>$formData['village'],
            'postal_code'=>$formData['postal_code'],
            'use_as'=>'main_address'
        ];

        $validBody['member_address'] = [$memberAddress];
        $validBody['status'] = 'ACTIVE';
        $validBody['input_form_data'] = $formData['form_input'];

        $CryptoViewModel = new CryptoViewModel();

        $password = $validBody['password'];
        $validBody['password'] = $CryptoViewModel->generatePassword($validBody['password']);

        $result = $MemberModel->insert($validBody);

        $returnResult = [
            'result'=>[
                'success'=>true,
                'data'=>[
                    'username'=>$validBody['username'],
                    'password'=>$password
                ]
            ]
        ];
        return $returnResult;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}