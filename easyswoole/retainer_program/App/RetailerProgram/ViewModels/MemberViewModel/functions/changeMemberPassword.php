<?php
namespace App\RetailerProgram\ViewModels\MemberViewModel;

use App\RetailerProgram\Models\MemberModel;
use App\GeneralData\ViewModels\CryptoViewModel;
use App\GeneralData\ViewModels\ReportViewModel;

function changeMemberPassword($arguments, $thisViewModel) {
    $auth = $arguments[0];
    $username = $arguments[1];

    try {

        $MemberModel = new MemberModel();
        $MemberModel->setCurrentUser($auth['user_id']);

        if (empty($username)) {
            return false;
        }

        $returnResult = [];
        foreach ($username as $key => $value) {
            $findMember = $MemberModel->findByUsernameOrId($value, 'ACTIVE');
            if (empty($findMember['result'])) {
                $thisViewModel->sendError("Member does not exists", 400);
            }

            $memberId = $findMember['result'][0]->_id;

            $CryptoViewModel = new CryptoViewModel();
            $password = generateAlphaNumericString(6, 3, 2);
            $hash = $CryptoViewModel->generatePassword($password);
            $result = $MemberModel->updateByID($memberId, ['password'=>$hash]);

            $returnResult[] = [
                'username'=>$findMember['result'][0]->username,
                'password'=>"'".$password
            ];
        }

        if (empty($returnResult)) {
            $result = ['result'=>['success'=>false]];
        } else {
            if (count($returnResult) > 1) {
                $ReportViewModel = new ReportViewModel();
                $result = $ReportViewModel->generateReport($returnResult, "CHANGE_PASSWORD");
            } else {
                $result = [
                    'result'=>[
                        'success'=>true,
                        'data'=>$returnResult[0]
                    ]
                ];
            }
        }
       
        return $result;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}