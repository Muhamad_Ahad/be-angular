<?php
namespace App\RetailerProgram\ViewModels\ProductViewModel;

use App\RetailerProgram\Models\ProductModel;
use App\RetailerProgram\Models\ProductSkuModel;

function addNewProduct($arguments, $thisViewModel) {
    $auth = $arguments[0];
    $bodyData = $arguments[1];

    try {

        $productData = $thisViewModel->validateDataProduct($bodyData);

        $ProductModel = new ProductModel();
        $ProductModel->setCurrentUser($auth['user_id']);

        $product = $ProductModel->findByProductCodeOrId($productData['product_code']);
        if (!empty($product['result'])) {
            $thisViewModel->sendError("product {$productData['product_code']} already exists", 404);
        }

        $ProductSkuModel = new ProductSkuModel();
        $ProductSkuModel->setCurrentUser($auth['user_id']);

        $productSku = $ProductSkuModel->findProductSku(['product_code'=>$productData['product_code']]);
        if (!empty($productSku['result'])) {
            $productSku = $thisViewModel->objectToArray($productSku['result']);
            $mapId = [];
            foreach ($productSku as $key => $value) {
                $mapId[] = $value['_id'];
            }

            $ProductSkuModel->removeByID($mapId);
        }

        $productSKU = $productData['product_sku'];
        unset($productData['product_sku']);
        $insertProduct = $ProductModel->insert($productData);

        foreach ($productSKU as $key => $value) {
            $productSKU[$key]['product_id'] = $insertProduct['result']['_id'];
            $productSKU[$key]['sku_code'] = $value['product_code'].'-'.$value['sku_code'];
        }

        $result = $ProductSkuModel->insertBatch($productSKU);
       
        return $result;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}