<?php
namespace App\RetailerProgram\ViewModels\ProductViewModel;

use App\RetailerProgram\Models\ProductModel;
use App\RetailerProgram\Models\ProductSkuModel;

function updateProduct($arguments, $thisViewModel) {
    $auth = $arguments[0];
    $bodyData = $arguments[1];

    try {

        $productData = $thisViewModel->validateDataProduct($bodyData);

        $ProductModel = new ProductModel();
        $ProductModel->setCurrentUser($auth['user_id']);

        $product = $ProductModel->findByProductCodeOrId($productData['product_code']);
        if (empty($product['result'])) {
            $thisViewModel->sendError("product {$productData['product_code']} does not exists", 404);
        }

        $productId = $ProductModel->convertToObjectId($product['result'][0]->_id);

        if (!empty($productData['product_sku'])) {
            $ProductSkuModel = new ProductSkuModel();
            $ProductSkuModel->setCurrentUser($auth['user_id']);

            $skuCode = [];
            $updateSku = [];
            foreach ($productData['product_sku'] as $key => $value) {

                $skuCode[] = $value['sku_code'];

                $value['product_id'] = $productId;

                $updateSku[] = [
                    'filter' => [
                        'deleted'=>['$exists'=>false],
                        'sku_code' => $value['sku_code'],
                        'product_code' => $value['product_code']
                    ],
                    'new_value' => $value
                ];
            }

            if (!empty($updateSku))
                $updateBulk = $ProductSkuModel->updateBatch($updateSku);

            $findSku = $ProductSkuModel->findProductSku(['product_code'=>$productData['product_code']]);
            if (!empty($findSku['result'])) {
                $findSku = $thisViewModel->objectToArray($findSku['result']);
                $mapId = [];
                foreach ($findSku as $key => $value) {
                    if (!in_array($value['sku_code'], $skuCode)) {
                        $mapId[] = $value['_id'];
                    }
                }

                $ProductSkuModel->removeByID($mapId);
            }

            unset($productData['product_sku']);
        }

        $updateProduct = $ProductModel->update(['_id'=>$productId], $productData);
       
        return $updateProduct;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}