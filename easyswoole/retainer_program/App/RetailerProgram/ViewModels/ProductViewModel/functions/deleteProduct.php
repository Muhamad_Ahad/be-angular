<?php
namespace App\RetailerProgram\ViewModels\ProductViewModel;

use App\RetailerProgram\Models\ProductModel;
use App\RetailerProgram\Models\ProductSkuModel;

function deleteProduct($arguments, $thisViewModel) {
    $auth = $arguments[0];
    $productCode = $arguments[1];
    try {

        $ProductModel = new ProductModel();
        $ProductModel->setCurrentUser($auth['user_id']);

        $ProductSkuModel = new ProductSkuModel();
        $ProductSkuModel->setCurrentUser($auth['user_id']);

        $findProduct = $ProductModel->findByProductCodeOrId($productCode);
        if (empty($findProduct['result'])) {
            $thisViewModel->sendError("Product {$productCode} does not exists", 404);
        }

        $deleteProduct = $ProductModel->removeByID($findProduct['result'][0]->_id);

        $findSku = $ProductSkuModel->findProductSku(['product_code'=>$productCode]);
        if (!empty($findSku['result'])) {
            $skuIds = [];
            foreach ($findSku['result'] as $key => $value) {
                $skuIds[] = $ProductSkuModel->convertToObjectId($value->_id);
            }

            if (!empty($skuIds)) {
                $filter = ['_id'=>['$in'=>$skuIds]];
                $deleteSku = $ProductSkuModel->DBdelete($filter);
            }
        }

        return $deleteProduct;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}