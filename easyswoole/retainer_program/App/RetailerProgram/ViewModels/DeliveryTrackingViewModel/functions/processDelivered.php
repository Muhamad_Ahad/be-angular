<?php
namespace App\RetailerProgram\ViewModels\DeliveryTrackingViewModel;

use App\RetailerProgram\Models\DeliveryTrackingModel;
use App\GeneralData\ViewModels\MediaViewModel;
use Services\SpreadsheetService;

function processDelivered($arguments, $thisViewModel) {
    $auth = $arguments[0];
    $method = $arguments[1];
    $bodyData = $arguments[2];

    try {

        switch ($method) {
            case 'single':
                $DeliveryTrackingModel = new DeliveryTrackingModel();
                $DeliveryTrackingModel->setCurrentUser($auth['user_id']);
                
                $filter = ['status'=>['ACTIVE', 'DELIVERED']];
                $delivery = $DeliveryTrackingModel->findByRefno($bodyData['ref_no'], $filter);

                empty($delivery['result']) ?
                    $thisViewModel->sendError("delivery data not available", 404) :
                    $delivery = $thisViewModel->objectToArray($delivery['result'][0]);

                if ($delivery['supported'] == 0) {
                    if (!preg_match("/^([0-9]{3}|[1-2][0-9]{3})-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1]) ([0-1][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$/i", $bodyData['delivered_date'])) {
                        $thisViewModel->sendError("Invalid date format {$bodyData['delivered_date']}, only \"YYYY-MM-DD HH:ii:ss\" format allowed", 400);
                    }
                    $date = createDateTime($bodyData['delivered_date'], 'format', "Y-m-d H:i:s");
                    $deliveredDate = $DeliveryTrackingModel->convertToMongoDateTime($date);

                    $currDate = date('Y-m-d H:i:s');
                    $createdDate = $currDate;

                    $shippingInfo = $delivery['shipping_info']?:[];
                    $lastShippingInfo = end($shippingInfo);
                    (!empty($lastShippingInfo) && !in_array($lastShippingInfo['label'], ['on_delivery', 'delivered', 'redelivery'])) ?
                        $thisViewModel->sendError("invalid operation", 400) :
                        null;

                    if ($lastShippingInfo['label'] == 'delivered'){
                        $createdDate = $lastShippingInfo['created_date'];
                        array_pop($shippingInfo);
                    }

                    $shippingInfo[] = [
                        'label'=>'delivered',
                        'title'=>'Delivered',
                        'receiver_name'=>$bodyData['receiver_name'],
                        'delivery_status'=>$bodyData['delivery_status'],
                        'remarks'=>$bodyData['remarks'],
                        'delivered_date'=>$deliveredDate,
                        'created_date'=>$createdDate,
                        'updated_date'=>$currDate
                    ];

                    $setData = [
                        'status'=>'DELIVERED',
                        'shipping_info'=>$shippingInfo
                    ];

                    foreach ($setData['shipping_info'] as $key => $value) {
                        empty($value['id']) ? :$value['id'] = $DeliveryTrackingModel->convertToObjectId($value['id']);
                        $value['created_date'] = $DeliveryTrackingModel->convertToMongoDateTime($value['created_date']);
                        $value['updated_date'] = $DeliveryTrackingModel->convertToMongoDateTime($value['updated_date']);
        
                        $setData['shipping_info'][$key] = $value;
                    }
                    $setData['shipping_info'] = array_values($setData['shipping_info']);

                    $returnResult = $DeliveryTrackingModel->updateByID($delivery['_id'], $setData);
                }
                break;
            
            case 'bulk':
                $MediaViewModel = new MediaViewModel();
                $document = $MediaViewModel->uploadDocument($auth, $bodyData, "PROCESS_DELIVERED");

                $SpreadsheetService = new SpreadsheetService();
                $files = $SpreadsheetService->convertToArray($document['file_path'], $document['ext']);

                $data = [
                    'user_id' => $auth['user_id'],
                    'files'=>array_values($files)
                ];
                if (!in_array($document['ext'], ['xls','xlsx'])) {
                    $thisViewModel->sendError("only .xls and .xlsx file type are allowed", 400);
                }
                $returnResult = processDeliveredBulk($data, $thisViewModel);
                break;
        }

        return $returnResult;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}

function processDeliveredBulk($data, &$thisViewModel) {
    
    try {
        $DeliveryTrackingModel = new DeliveryTrackingModel();
        $DeliveryTrackingModel->setCurrentUser($data['user_id']);

        $requiredField = explode(",", "ref_no,delivered_date,receiver_name,delivery_status");
        $currDate = date('Y-m-d H:i:s');
        $updateData = [];
        foreach ($data['files'] as $key => $value) {
            $row = $key + 1;
            foreach ($requiredField as $k => $val) {
                empty($value[$val])? $thisViewModel->sendError("field {$val} on row {$row} is required", 400): null;   
            }

            $delivery = $DeliveryTrackingModel->findByRefno($value['ref_no'], ['status'=>'ACTIVE']);
            empty($delivery['result']) ?
                $thisViewModel->sendError("delivery data not available", 404) :
                $delivery = $thisViewModel->objectToArray($delivery['result'][0]);

            if ($delivery['supported'] == 0) {
                if (!preg_match("/^([0-9]{3}|[1-2][0-9]{3})-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1]) ([0-1][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$/i", $value['delivered_date'])) {
                    $thisViewModel->sendError("Invalid date format {$value['delivered_date']}, only \"YYYY-MM-DD HH:ii:ss\" format allowed", 400);
                }
                $date = createDateTime($value['delivered_date'], 'format', "Y-m-d H:i:s");
                $deliveredDate = $DeliveryTrackingModel->convertToMongoDateTime($date);

                $currDate = date('Y-m-d H:i:s');
                $createdDate = $currDate;

                $shippingInfo = $delivery['shipping_info']?:[];
                $lastShippingInfo = end($shippingInfo);
                (!empty($lastShippingInfo) && !in_array($lastShippingInfo['label'], ['on_delivery', 'delivered','redelivery'])) ?
                    $thisViewModel->sendError("invalid operation", 400) :
                    null;

                if ($lastShippingInfo['label'] == 'delivered'){
                    $createdDate = $lastShippingInfo['created_date'];
                    array_pop($shippingInfo);
                }

                $shippingInfo[] = [
                    'label'=>'delivered',
                    'title'=>'Delivered',
                    'receiver_name'=>$value['receiver_name'],
                    'delivery_status'=>$value['delivery_status'],
                    'remarks'=>$value['remarks'],
                    'delivered_date'=>$deliveredDate,
                    'created_date'=>$createdDate,
                    'updated_date'=>$currDate
                ];

                $setData = [
                    'status'=>'DELIVERED',
                    'shipping_info'=>$shippingInfo
                ];

                foreach ($setData['shipping_info'] as $sk => $sval) {
                        empty($value['id']) ? :$value['id'] = $DeliveryTrackingModel->convertToObjectId($value['id']);
                        $sval['created_date'] = $DeliveryTrackingModel->convertToMongoDateTime($sval['created_date']);
                    $sval['updated_date'] = $DeliveryTrackingModel->convertToMongoDateTime($sval['updated_date']);
    
                    $setData['shipping_info'][$sk] = $sval;
                }
                $setData['shipping_info'] = array_values($setData['shipping_info']);

                $updateData[] = [
                    'filter'=>['_id'=>$DeliveryTrackingModel->convertToObjectId($delivery['_id'])],
                    'new_value'=>$setData
                ];
            }
        }

        if (!empty($updateData)) {
            $result = $DeliveryTrackingModel->updateBatch($updateData);
        }

        return $result;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}