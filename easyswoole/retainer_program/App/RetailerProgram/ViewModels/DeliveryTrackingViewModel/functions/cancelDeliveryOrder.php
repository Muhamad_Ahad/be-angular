<?php
namespace App\RetailerProgram\ViewModels\DeliveryTrackingViewModel;

use App\RetailerProgram\Models\DeliveryTrackingModel;

function cancelDeliveryOrder($arguments, $thisViewModel) {
    $auth = $arguments[0];
    $orderId = $arguments[1];

    try {
        $DeliveryTrackingModel = new DeliveryTrackingModel();
        $DeliveryTrackingModel->setCurrentUser($auth['user_id']);

        $delivery = $DeliveryTrackingModel->DBfind(['order_id'=>$orderId]);

        if (!empty($delivery['result'])) {
            $delivery = $thisViewModel->objectToArray($delivery['result']);

            foreach ($delivery as $key => $value) {
                $lastShippingInfo = end($value['shipping_info']);
                if ($value['status'] == 'DELIVERED' || $lastShippingInfo['label'] == 'delivered')
                    $thisViewModel->sendError("Package with reference_no {$value['reference_no']} already delivered", 400);

                if ($lastShippingInfo['label'] == 'on_delivery') 
                    $thisViewModel->sendError("Package with reference_no {$value['reference_no']} is on delivery", 400);
            }

            $DeliveryTrackingModel->DBupdate(['order_id'=>$orderId], ['status'=>'CANCELLED']);
        }

    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
