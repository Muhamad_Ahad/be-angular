<?php
namespace App\RetailerProgram\ViewModels\DeliveryTrackingViewModel;

use App\RetailerProgram\Models\DeliveryTrackingModel;
use App\GeneralData\ViewModels\CourierViewModel;
use App\GeneralData\ViewModels\MediaViewModel;
use Services\SpreadsheetService;

function cancelPickupRequest($arguments, $thisViewModel) {
    $auth = $arguments[0];
    $method = $arguments[1];
    $bodyData = $arguments[2];

    try {

        switch ($method) {
            case 'single':
                $DeliveryTrackingModel = new DeliveryTrackingModel();
                $DeliveryTrackingModel->setCurrentUser($auth['user_id']);
                
                $filter = [
                    'status'=>'ACTIVE',
                    'shipping_info.label'=>'on_delivery'
                ];
                $delivery = $DeliveryTrackingModel->findByRefno($bodyData['ref_no'], $filter);

                empty($delivery['result']) ?
                    $thisViewModel->sendError("delivery data not available", 404) :
                    $delivery = $thisViewModel->objectToArray($delivery['result'][0]);

                $shippingInfo = $delivery['shipping_info']?:[];
                $lastShippingInfo = end($shippingInfo);
                (empty($lastShippingInfo['label']) || $lastShippingInfo['label'] != 'on_delivery') ?
                    $thisViewModel->sendError("invalid operation", 400) : null;

                (!empty($delivery['track_history'])) ?
                    $thisViewModel->sendError("Package already picked up by courier", 400) : null;

                if ($delivery['supported']) {
                    $CourierViewModel = new CourierViewModel();
                    $params = [
                        'reference_no'=>$delivery['reference_no'],
                        'awb_number'=>$delivery['awb_number'],
                        'description'=>$bodyData['description']
                    ];
                    $cancelPickup = $CourierViewModel->cancelCourierPickup($delivery['courier'], $params);
                }

                array_pop($delivery['shipping_info']);
                $setData = [
                    'shipping_info'=>$delivery['shipping_info'],
                    'courier'=>'',
                    'courier_name'=>'',
                    'delivery_method'=>'',
                    'delivery_service'=>'',
                    'supported'=>-1,
                    'delivery_cost'=>0,
                    'track_history'=>[],
                    'awb_number'=>'',
                    'additional_info'=>[],
                ];

                foreach ($setData['shipping_info'] as $key => $value) {
                    $value['created_date'] = $DeliveryTrackingModel->convertToMongoDateTime($value['created_date']);
                    $value['updated_date'] = $DeliveryTrackingModel->convertToMongoDateTime($value['updated_date']);
    
                    $setData['shipping_info'][$key] = $value;
                }

                $returnResult = $DeliveryTrackingModel->updateByID($delivery['_id'], $setData);
                break;
            
            case 'bulk':
                $MediaViewModel = new MediaViewModel();
                $document = $MediaViewModel->uploadDocument($auth, $bodyData, "CANCEL_PICKUP");

                $SpreadsheetService = new SpreadsheetService();
                $files = $SpreadsheetService->convertToArray($document['file_path'], $document['ext']);

                $data = [
                    'user_id' => $auth['user_id'],
                    'files'=>array_values($files)
                ];
                if (!in_array($document['ext'], ['xls','xlsx'])) {
                    $thisViewModel->sendError("only .xls and .xlsx file type are allowed", 400);
                }
                $returnResult = cancelPickupBulk($data, $thisViewModel);
                break;
        }

        return $returnResult;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}

function cancelPickupBulk($data, &$thisViewModel) {
    try {

        $DeliveryTrackingModel = new DeliveryTrackingModel();
        $DeliveryTrackingModel->setCurrentUser($data['user_id']);

        $CourierViewModel = new CourierViewModel();

        $updateData = [];
        foreach ($data['files'] as $key => $value) {
            empty($value['ref_no']) ? $thisViewModel->sendError("field ref_no is required", 400) : null;

            $filter = [
                'status'=>'ACTIVE',
                'shipping_info.label'=>'on_delivery'
            ];
            $delivery = $DeliveryTrackingModel->findByRefno($value['ref_no'], $filter);

            empty($delivery['result']) ?
                $thisViewModel->sendError("delivery data not available", 404) :
                $delivery = $thisViewModel->objectToArray($delivery['result'][0]);

            $shippingInfo = $delivery['shipping_info']?:[];
            $lastShippingInfo = end($shippingInfo);
            (empty($lastShippingInfo['label']) || $lastShippingInfo['label'] != 'on_delivery') ?
                $thisViewModel->sendError("invalid operation", 400) : null;

            (!empty($delivery['track_history'])) ?
                $thisViewModel->sendError("Package with ref_no {$value['ref_no']} already picked up by courier", 400) : null;

            if ($delivery['supported']) {
                $params = [
                    'reference_no'=>$delivery['reference_no'],
                    'awb_number'=>$delivery['awb_number'],
                    'description'=>$value['description']
                ];
                $cancelPickup = $CourierViewModel->cancelCourierPickup($delivery['courier'], $params);
            }

            array_pop($delivery['shipping_info']);

            foreach ($delivery['shipping_info'] as $k => $val) {
                $val['created_date'] = $DeliveryTrackingModel->convertToMongoDateTime($val['created_date']);
                $val['updated_date'] = $DeliveryTrackingModel->convertToMongoDateTime($val['updated_date']);

                $delivery['shipping_info'][$k] = $val;
            }

            $delivery['shipping_info'] = array_values($delivery['shipping_info']);

            $updateData[] = [
                'filter'=>['_id'=>$DeliveryTrackingModel->convertToObjectId($delivery['_id'])],
                'new_value'=>[
                    'shipping_info'=>$delivery['shipping_info'],
                    'courier'=>'',
                    'courier_name'=>'',
                    'delivery_method'=>'',
                    'delivery_service'=>'',
                    'supported'=>-1,
                    'delivery_cost'=>0,
                    'track_history'=>[],
                    'awb_number'=>'',
                    'additional_info'=>[],
                ]
            ];
        }

        !empty($updateData)?
            $result = $DeliveryTrackingModel->updateBatch($updateData):
            $result = [];

        return $result;

    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}