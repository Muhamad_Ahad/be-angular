<?php
namespace App\RetailerProgram\ViewModels\DeliveryTrackingViewModel;

use App\RetailerProgram\Models\DeliveryTrackingModel;
use App\GeneralData\ViewModels\ReportViewModel;

function findAllReport($arguments, $thisViewModel) {
    $search = $arguments[0];
    $order_by = $arguments[1];
    $page = $arguments[2];
    $limit_per_page = $arguments[3];
    $requested_columns = $arguments[4];
    $download = $arguments[5];

    try {

        $DeliveryTrackingModel = new DeliveryTrackingModel();
        // utk urutan output
        if (!isset($requested_columns)) {
            $requested_columns = "origin,destination,product_list,total_item_value,total_item_qty,order_id,reference_no,dimensions,weight,volumetric_weight,shipping_info,last_shipping_info,status,courier,courier_name,supported,available_courier,delivery_type,delivery_method,delivery_service,awb_number,delivery_cost,track_history,created_date,updated_date,additional_info,packing_no,return_detail, redelivery_detail";
        }
        $requested_columns = explode(",", $requested_columns);

        $DeliveryTrackingModel->requestColumns .= ',last_shipping_info';
        $DeliveryTrackingModel->requestMapping['origin'] = '$origin.address';
        $DeliveryTrackingModel->requestMapping['destination'] = '$destination.address';
        $DeliveryTrackingModel->requestMapping['last_shipping_info'] = '$last_shipping_info';

        if ($download) {
            $result_db = $DeliveryTrackingModel->findAllReport($search, $order_by, $page, null, $requested_columns);
            $result_db = json_decode(json_encode($result_db['result']['values']), 1);

            if (count($result_db) == 0) {
                $thisViewModel->sendResult('Empty result founded, No need to convert it as Excel / Csv Spread sheet', 400);
            } else {
                $ReportViewModel = new ReportViewModel();

                $result = $ReportViewModel->generateReport($result_db, "DELIVERY_TRACK_REPORT");
            }
            
        } else {
            $result = $DeliveryTrackingModel->findAllReport($search, $order_by, $page, $limit_per_page, $requested_columns);
            $result['result']['values'] = json_decode(json_encode($result['result']['values']), 1);
        }

        return $result;

    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}