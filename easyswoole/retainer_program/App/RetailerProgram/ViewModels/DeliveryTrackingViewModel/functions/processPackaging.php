<?php
namespace App\RetailerProgram\ViewModels\DeliveryTrackingViewModel;

use App\RetailerProgram\Models\DeliveryTrackingModel;
use App\GeneralData\ViewModels\CourierViewModel;
use App\GeneralData\ViewModels\MediaViewModel;
use App\RetailerProgram\ViewModels\ProductViewModel;
use Services\SpreadsheetService;


function processPackaging($arguments, $thisViewModel) {
    $auth = $arguments[0];
    $method = $arguments[1];
    $bodyData = $arguments[2];

    try {

        switch ($method) {
            case 'single':
                $DeliveryTrackingModel = new DeliveryTrackingModel();
                $DeliveryTrackingModel->setCurrentUser($auth['user_id']);

                $ProductViewModel = new ProductViewModel();
                
                $filter = ['status'=>['PENDING','ACTIVE']];
                $delivery = $DeliveryTrackingModel->findByRefno($bodyData['ref_no'], $filter);

                empty($delivery['result']) ?
                    $thisViewModel->sendError("delivery data not available", 404) :
                    $delivery = $thisViewModel->objectToArray($delivery['result'][0]);

                $currDate = date('Y-m-d H:i:s');
                $currDate = $DeliveryTrackingModel->convertToMongoDateTime($currDate);
                $createdDate = $currDate;

                $shippingInfo = $delivery['shipping_info']?:[];
                $lastShippingInfo = end($shippingInfo);
                (!empty($lastShippingInfo) && $lastShippingInfo['label'] != 'on_processing') ?
                    $thisViewModel->sendError("invalid operation", 400) :
                    $createdDate = $DeliveryTrackingModel->convertToMongoDateTime($lastShippingInfo['created_date']);;

                empty($bodyData['weight']) ? 
                    $thisViewModel->sendError('package weight is required', 400):
                    $bodyData['weight'] = floatval($bodyData['weight']/1000);
                $dimensions = $bodyData['dimensions'];
                $validDimensions = $ProductViewModel->calculateVolumetricWeight($dimensions['length'], $dimensions['width'], $dimensions['height']);

                $setData = [
                    'status'=>'ACTIVE',
                    'dimensions'=>$validDimensions['dimensions'],
                    'weight'=>$bodyData['weight'],
                    'volumetric_weight'=>$validDimensions['volume_weight']/1000,
                    'shipping_info'=>[
                        [
                            'label'=>'on_processing',
                            'title'=>'On Packaging and Procesing',
                            'created_date'=>$createdDate,
                            'updated_date'=>$currDate
                        ]
                    ]
                ];

                if (!empty($delivery['origin']['code']) && !empty($delivery['destination']['code'])) {
                    ( $setData['weight'] >= $setData['volumetric_weight'] ) ?
                        $weight = $setData['weight'] :
                        $weight = $setData['volumetric_weight'];

                    ($delivery['delivery_type'] == 'gold') ?
                        $typeSpecial = true : $typeSpecial = false;

                    $CourierViewModel = new CourierViewModel();
                    $availableCourier = $CourierViewModel->getAvailableCourier($delivery['origin'], $delivery['destination'], $weight, $typeSpecial, $delivery['total_item_value']);
                    $setData['available_courier'] = $availableCourier?:[];
                }

                $returnResult = $DeliveryTrackingModel->updateByID($delivery['_id'], $setData);
                break;
            
            case 'bulk':
                $MediaViewModel = new MediaViewModel();
                $document = $MediaViewModel->uploadDocument($auth, $bodyData, "PROCESS_PACKAGING");

                $SpreadsheetService = new SpreadsheetService();
                $files = $SpreadsheetService->convertToArray($document['file_path'], $document['ext']);

                $data = [
                    'user_id' => $auth['user_id'],
                    'files'=>array_values($files)
                ];
                if (!in_array($document['ext'], ['xls','xlsx'])) {
                    $thisViewModel->sendError("only .xls and .xlsx file type are allowed", 400);
                }
                $returnResult = processPackagingBulk($data, $thisViewModel);
                break;
        }

        return $returnResult;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}

function processPackagingBulk($data, &$thisViewModel) {

    try {

        $DeliveryTrackingModel = new DeliveryTrackingModel();
        $DeliveryTrackingModel->setCurrentUser($data['user_id']);

        $CourierViewModel = new CourierViewModel();
        $ProductViewModel = new ProductViewModel();

        $currDate = date('Y-m-d H:i:s');
        $currDate = $DeliveryTrackingModel->convertToMongoDateTime($currDate);
        
        $requiredField = explode(",", "ref_no,weight,length,width,height");
        $updateData = [];
        foreach ($data['files'] as $key => $value) {
            $row = $key + 1;
            foreach ($requiredField as $k => $val) {
                if (empty($value[$val]))
                    $thisViewModel->sendError("field {$val} on row {$row} is required", 400);
            }
            $filter = ['status'=>['PENDING','ACTIVE']];
            $delivery = $DeliveryTrackingModel->findByRefno($value['ref_no'], $filter);

            empty($delivery['result']) ?
                $thisViewModel->sendError("delivery data for ref no {$value['ref_no']} does not exists", 404) :
                $delivery = $thisViewModel->objectToArray($delivery['result'][0]);

            $createdDate = $currDate;

            $shippingInfo = $delivery['shipping_info']?:[];
            $lastShippingInfo = end($shippingInfo);
            (!empty($lastShippingInfo) && $lastShippingInfo['label'] != 'on_processing') ?
                $thisViewModel->sendError("Invalid shipping label for ref no {$value['ref_no']}", 400) :
                $createdDate = $DeliveryTrackingModel->convertToMongoDateTime($lastShippingInfo['created_date']);;

            empty($value['weight']) ? 
                $thisViewModel->sendError('package weight on row {$row} is required', 400):
                $value['weight'] = floatval($value['weight'])/1000;

            $validDimensions = $ProductViewModel->calculateVolumetricWeight($value['length'], $value['width'], $value['height']);
            
            $setData = [
                'status'=>'ACTIVE',
                'dimensions'=>$validDimensions['dimensions'],
                'weight'=>$value['weight'],
                'volumetric_weight'=>$validDimensions['volume_weight']/1000,
                'shipping_info'=>[
                    [
                        'label'=>'on_processing',
                        'title'=>'On Packaging and Procesing',
                        'created_date'=>$createdDate,
                        'updated_date'=>$currDate
                    ]
                ]
            ];

            if (!empty($delivery['origin']['code']) && !empty($delivery['destination']['code'])) {
                ( $setData['weight'] >= $setData['volumetric_weight'] ) ?
                    $weight = $setData['weight'] :
                    $weight = $setData['volumetric_weight'];

                ($delivery['delivery_type'] == 'gold') ?
                    $typeSpecial = true : $typeSpecial = false;

                $CourierViewModel = new CourierViewModel();
                $availableCourier = $CourierViewModel->getAvailableCourier($delivery['origin']['code'], $delivery['destination']['code'], $weight, $typeSpecial, $delivery['total_item_value']);
                $setData['available_courier'] = $availableCourier?:[];
            }

            $updateData[] = [
                'filter'=>['_id'=>$DeliveryTrackingModel->convertToObjectId($delivery['_id'])],
                'new_value'=>$setData
            ];
        }

        $returnResult = $DeliveryTrackingModel->updateBatch($updateData);

        return $returnResult;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}