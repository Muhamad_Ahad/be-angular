<?php
namespace App\RetailerProgram\ViewModels\EvoucherViewModel;

use App\RetailerProgram\Models\EvoucherModel;
use App\RetailerProgram\Models\MemberModel;

function findMemberEvoucherLists($arguments, $thisViewModel) {
    $auth = $arguments[0];
    $option = $arguments[1]?:'available';
    $username = $arguments[2];

    try {

        $EvoucherModel = new EvoucherModel();
        if ($auth['type'] == 'member') {
            $userId = $EvoucherModel->convertToObjectId($auth['subject_id']);
        } elseif ($auth['type'] == 'admin') {
            empty($username) ? 
                $thisViewModel->sendError("member username is required", 400) : null;

            $MemberModel = new MemberModel();
            $member = $MemberModel->findByUsernameOrId($username);
            empty($member['result']) ?
                $thisViewModel->sendError("member does not exists", 404) : null;

            $userId = $EvoucherModel->convertToObjectId($member['result'][0]->_id);
        }

        $result = $EvoucherModel->findEvoucherLists(['owner_id'=>$userId], ['option'=>$option]);
        return $result;

    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}