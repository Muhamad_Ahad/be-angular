<?php
namespace App\RetailerProgram\ViewModels\EvoucherViewModel;

use App\RetailerProgram\Models\EvoucherModel;
use App\GeneralData\ViewModels\CryptoViewModel;

function getEvoucherDetail($arguments, $thisViewModel) {
    $auth = $arguments[0];
    $processId = $arguments[1];

    try {
        $CryptoViewModel = new CryptoViewModel();

        $EvoucherModel = new EvoucherModel();

        $EvoucherModel->requestMapping = array_merge(
            $EvoucherModel->requestMapping,
            [
                '_id'=>null,
                'product_id'=>null,
                'sku_id'=>null,
                'owner_id'=>null
            ]
        );
        
        $filter = [
            'owner_id' => $EvoucherModel->convertToObjectId($auth['subject_id']),
            'process_id' => $processId
        ];
        $evoucher = $EvoucherModel->findEvoucher($filter);
        if (empty($evoucher['result'])) {
            $thisViewModel->sendError("Evoucher does not exists", 404);
        }
        $evoucher = $thisViewModel->objectToArray($evoucher['result'][0]);
        $evoucher['voucher_code'] = $CryptoViewModel->sslDecrypt($evoucher['voucher_code']);

        !empty($evoucher['redeem_url'])?
            $evoucher['redeem_url'] = $CryptoViewModel->sslDecrypt($evoucher['redeem_url']):
            null;

        if (empty($evoucher['barcode']) || empty($evoucher['qr_code'])) {
            $label = $thisViewModel->generateEvoucherLabel($auth, $evoucher['voucher_code']);

            $setData = [
                'barcode'=>$CryptoViewModel->sslEncrypt($label['barcode']),
                'qr_code'=>$CryptoViewModel->sslEncrypt($label['qr_code'])
            ];
            $updateLabel = $EvoucherModel->update($filter, $setData);

            $evoucher['barcode'] = $label['barcode'];
            $evoucher['qr_code'] = $label['qr_code'];
        } else {
            $evoucher['barcode'] = $CryptoViewModel->sslDecrypt($evoucher['barcode']);
            $evoucher['qr_code'] = $CryptoViewModel->sslDecrypt($evoucher['qr_code']);
        }
        unset($evoucher['_id']);

        $result = ['result'=>$evoucher];
        return $result;

    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}