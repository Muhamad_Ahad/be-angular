<?php
namespace App\RetailerProgram\ViewModels\EvoucherViewModel;

use App\GeneralData\ViewModels\LabelViewModel;
use App\GeneralData\ViewModels\MediaViewModel;

function generateEvoucherLabel($arguments, $thisViewModel) {
    $auth = $arguments[0];
    $evoucherCode = $arguments[1];

    try {
        $LabelViewModel = new LabelViewModel();
        $MediaViewModel = new MediaViewModel();

        $barcode = $LabelViewModel->generateBarcode($evoucherCode, 'jpeg');
        $qrCode = $LabelViewModel->generateQrCode($evoucherCode, 'jpeg');

        $barcodeImage = [
            'name'=>$barcode['file_name'],
            'tmp_name'=>$barcode['file_path'],
            'size'=>filesize($barcode['file_path'])
        ];
        $uploadBarcode = $MediaViewModel->uploadImage($auth, $barcodeImage)['result'];

        $qrcodeImage = [
            'name'=>$qrCode['file_name'],
            'tmp_name'=>$qrCode['file_path'],
            'size'=>filesize($qrCode['file_path'])
        ];
        $uploadQrcode = $MediaViewModel->uploadImage($auth, $qrcodeImage)['result'];

        unlink($barcode['file_path']);
        unlink($qrCode['file_path']);
        $result = [
            'barcode'=>$uploadBarcode['base_url'].$uploadBarcode['pic_medium_path'],
            'qr_code'=>$uploadQrcode['base_url'].$uploadQrcode['pic_medium_path']
        ];
        
        return $result;

    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}