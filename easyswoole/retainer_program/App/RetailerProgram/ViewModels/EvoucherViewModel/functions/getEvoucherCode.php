<?php
namespace App\RetailerProgram\ViewModels\EvoucherViewModel;

use App\RetailerProgram\Models\EvoucherModel;
use App\GeneralData\ViewModels\CryptoViewModel;
use App\GeneralData\ViewModels\ReportViewModel;

function getEvoucherCode($arguments, $thisViewModel) {
    $auth = $arguments[0];
    $search = $arguments[1];
    $order_by = $arguments[2];
    $page = $arguments[3];
    $limit_per_page = $arguments[4];
    $requested_columns = $arguments[5];
    // $download = $arguments[6];

    try {

        $EvoucherModel = new EvoucherModel();
        // utk urutan output
        if (empty($requested_columns)) {
            $requested_columns = "product_code,sku_code,voucher_code,expiry_date,reference_no,process_number,process_id,status";
        }
        $EvoucherModel->requestMapping['_id'] = 0;

        ($auth['type'] == 'member') ?
            $search['owner_id'] = ['id'=>$auth['subject_id']] : null;
        
        if ($auth['type'] == 'admin') {
            $search['get_detail'] = true;
            $EvoucherModel->requestColumns .= ',full_name,username,cell_phone,nama_pemilik,nama_penerima';
            $EvoucherModel->requestMapping = array_merge(
                $EvoucherModel->requestMapping,
                [
                    'full_name'=>'$full_name',
                    'username'=>'$username',
                    'cell_phone'=>'$cell_phone',
                    'nama_pemilik'=>'$nama_pemilik',
                    'nama_penerima'=>'$nama_penerima'
                ]
            );
            $requested_columns .= ',full_name,username,cell_phone,nama_pemilik,nama_penerima';
        }
        $requested_columns = explode(",", $requested_columns);

        $search['status'] = "RESERVED";
        $result_db = $EvoucherModel->findAllReport($search, $order_by, $page, $limit_per_page, $requested_columns);

        if (empty($result_db['result']['values'])) {
            $thisViewModel->sendError('Empty result founded, No need to convert it as Excel / Csv Spread sheet', 400);
        } else {

            $result_db = json_decode(json_encode($result_db['result']['values']), 1);

            $CryptoViewModel = new CryptoViewModel();
            foreach ($result_db as $key => $value) {
                $result_db[$key]['voucher_code'] = $CryptoViewModel->sslDecrypt($value['voucher_code']);

                !empty($value['redeem_url'])?
                    $result_db[$key]['redeem_url'] = $CryptoViewModel->sslDecrypt($value['redeem_url']):
                    null;
            }

            $ReportViewModel = new ReportViewModel();
            $result = $ReportViewModel->generateReport($result_db, "EVOUCHER_CODE_REPORT");
        }

        return $result;

    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}