<?php
namespace App\RetailerProgram\ViewModels\CategoryViewModel;

use App\RetailerProgram\Models\CategoryModel;

function find($arguments, $thisViewModel)
{
    $search = $arguments[0];
    $order_by = $arguments[1];
    $page = $arguments[2];
    $limit_per_page = $arguments[3];
    $request_column = $arguments[4];
    
    try {
        // utk urutan output
        if (!isset($request_column)) {
            $request_column = "name,code,imageUrl,key,type";
        }
        $request_column = explode(",", $request_column);

        $CategoryModel = new CategoryModel();
        
        $result = $CategoryModel->findAllReport($search, $order_by, $page, $limit_per_page, $request_column);
        $result['result']['values'] = json_decode(json_encode($result['result']['values']), 1);
        return $result;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}
