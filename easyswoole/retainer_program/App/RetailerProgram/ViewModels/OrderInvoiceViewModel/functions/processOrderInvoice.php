<?php
namespace App\RetailerProgram\ViewModels\OrderInvoiceViewModel;

use App\RetailerProgram\Models\OrderInvoiceModel;
use App\RetailerProgram\ViewModels\EvoucherViewModel;
use App\RetailerProgram\ViewModels\DeliveryTrackingViewModel;

function processOrderInvoice($arguments, $thisViewModel) {
    $auth = $arguments[0];
    $bodyData = $arguments[1];

    try {
        $OrderInvoiceModel = new OrderInvoiceModel();
        $OrderInvoiceModel->setCurrentUser($auth['user_id']);

        $invoice = $OrderInvoiceModel->findByOrderId($bodyData['order_id'], ['status'=>'PENDING']);
        empty($invoice['result']) ?
            $thisViewModel->sendError("order data not found", 404) :
            $invoice = $thisViewModel->objectToArray($invoice['result'][0]);

        $userId = $OrderInvoiceModel->convertToObjectId($auth['user_id']);

        $currDate = $OrderInvoiceModel->convertToMongoDateTime(date('Y-m-d H:i:s'));
        
        $EvoucherViewModel = new EvoucherViewModel();
        $EvoucherViewModel->reserveEvoucher($auth, $invoice['user_id'], $invoice['order_id']);

        $shipping = false;
        foreach ($invoice['products'] as $key => $value) {
            if ($value['type'] == 'product' || $value['type'] == 'voucher' || $value['type'] == 'gold'){
                $shipping = true;
                break;
            }
        }

        $setData = [
            'status'=>'PROCESSED',
            // 'shipping_info'=>[],
            'approve_date'=>$currDate,
            'approve_at'=>getClientIpAddr(),
            'approve_by'=>$userId
        ];
        
        if ($shipping) {
            $DeliveryTrackingViewModel = new DeliveryTrackingViewModel();

            $DeliveryTrackingViewModel->createDeliveryOrder($auth, $invoice);
        }

        $result = $OrderInvoiceModel->updateByID($invoice['_id'], $setData);
        
        return $result;
    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}