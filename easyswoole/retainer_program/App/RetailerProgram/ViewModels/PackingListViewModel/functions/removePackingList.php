<?php
namespace App\RetailerProgram\ViewModels\PackingListViewModel;

use App\RetailerProgram\Models\PackingListModel;
use App\RetailerProgram\Models\DeliveryTrackingModel;

function removePackingList($arguments, $thisViewModel) {
    $auth = $arguments[0];
    $packingNo = $arguments[1];
    $referenceNo = $arguments[2];

    try{

        $DeliveryTrackingModel = new DeliveryTrackingModel();
        $DeliveryTrackingModel->setCurrentUser($auth['user_id']);
        $PackingListModel = new PackingListModel();
        $PackingListModel->setCurrentUser($auth['user_id']);

        $filter = [
            'packing_no'=>$packingNo,
            'status'=>'ACTIVE'
        ];
        $getPackingList = $PackingListModel->DBfind($filter);

        empty($getPackingList['result']) ?
            $thisViewModel->sendResult("packing no {$packingNo} is not available", 404) :
            $getPackingList = $thisViewModel->objectToArray($getPackingList['result'][0]);

        $updateData = [];
        foreach ($referenceNo as $key => $value) {

            if (in_array($value, $getPackingList['reference_no'])) {

                $filter = [
                    'reference_no'=>$value,
                    'shipping_info'=>['$exists'=>true],
                    'shipping_info.label'=>'on_delivery',
                    'status'=>'ACTIVE',
                    'packing_no'=>$packingNo,
                    'in_packing_list'=>true
                ];
                $delivery = $DeliveryTrackingModel->DBfind($filter);
    
                empty($delivery['result']) ?
                    $thisViewModel->sendError("cannot process order {$value}", 400) : 
                    $delivery = $thisViewModel->objectToArray($delivery['result'][0]);

                $arrKey = array_keys($getPackingList['reference_no'], $value);
                unset($getPackingList['reference_no'][$arrKey[0]]);

                $updateData[] = [
                    'filter' => ['_id'=>$DeliveryTrackingModel->convertToObjectId($delivery['_id'])],
                    'new_value'=>[
                        'packing_no'=>"",
                        "in_packing_list"=>false
                    ]
                ];
            }
        }

        if (!empty($updateData)) {
            $DeliveryTrackingModel->updateBatch($updateData);

            $getPackingList['reference_no'] = array_values($getPackingList['reference_no']);

            $updatePackingList = $PackingListModel->updateByID($getPackingList['_id'], ['reference_no'=>$getPackingList['reference_no']]);
        }

        $result = [
            'result' => ['status'=>'success']
        ];

        return $result;

    } catch (\Exception $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    } catch (\Error $e) {
        $thisViewModel->sendError($e->getMessage(), $e->getCode(), $e);
    }
}