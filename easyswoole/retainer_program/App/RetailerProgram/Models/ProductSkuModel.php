<?php
namespace App\RetailerProgram\Models;

use Infrastructure\MongoModel;

class ProductSkuModel extends MongoModel {
    public $table;
    public $dbName;
    public $collectionName = "dtm_product_sku";

    public $requestColumns = "_id,sku_code,sku_price,sku_value,product_id,product_code,variant,qty,image_gallery,status,created_date,updated_date,deleted_date,product_info,count";
    public $requestMapping = [
        '_id' => '$_id',
        'sku_code'=>'$sku_code',
        'sku_price'=>'$sku_price',
        'sku_value'=>'$sku_value',
        'product_id'=>'$product_id',
        'product_code'=>'$product_code',
        'variant'=>'$variant',
        'qty'=>'$qty',
        'image_gallery'=>'$image_gallery',
        'status'=>'$status',
        'product_info'=>'$product_info',
        'created_date'=>'$created_date',
        'updated_date'=>'$updated_date',
        'deleted_date'=>'$deleted_date',
    ];

    function findAllReport($request, $orderBy = ['_id'=>-1], $pageNo = 1, $limitPerPage = 40, $requestedColumns = []) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            $limit = $limitPerPage ? $limitPerPage : false;
            $pageNo = $pageNo ? $pageNo : 1;

            if ($limit) {
                $options = $this->parsePageToSkip($pageNo, $limit);
                $skip = $options['skip'];
            }
            $columnMapping = $this->columnsMapping($requestedColumns);

            $requestMatch = [];
            foreach ($request as $key => $column) {
                if (isset($this->requestMapping[$key])) {
                    if ($key == '_id'){
                        $requestMatch['_id'] = $this->convertToObjectId($column);
                    } else {
                        $request_mapping = substr($this->requestMapping[$key], 1);
                        $requestMatch[$request_mapping] = $this->convertValueToMongoQuery($column);
                    }
                } elseif ($key == '_id' && $column == 0) $requestMatch[$key] = 0;
            }

            $pipeline = [];
            $pipeline[] = ['$project' => $columnMapping];
            if (!empty($requestMatch)) {
                $pipeline[] = ['$match' => $requestMatch];
            }

            $pipeline[] = empty($orderBy) ? ['$sort' => ['_id' => -1]] : ['$sort' => $orderBy];

            if ($limit) {
                $pipeline[] = ['$skip' => $skip];
                $pipeline[] = ['$limit' => $limit];
            }

            $return = $this->DBaggregate($pipeline);
            unset($pipeline[2]);
            unset($pipeline[3]);
            unset($pipeline[4]);

            $countPipeline = $pipeline;
            $countPipeline[2] = [
                '$group' => [
                    '_id' => null,
                    'count' => ['$sum' => 1],
                ],
            ];

            $totAggDB = $this->DBaggregate($countPipeline);
            $total = $totAggDB['result'][0]->count;

            if ($limit) {
                $total_page = ceil(intval($total) / $limit);
            } else {
                $total_page = 1;
            }

            $result = [
                'values' => $return['result'],
                    'total_all_values' => $total,
                    'total_page' => $total_page,
                    'skip' => $skip,
            ];

            return $this->sendResult($result);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function find($filter) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            $match = [];
            foreach ($filter as $key => $column) {
                if (isset($this->requestMapping[$key])) {
                    $request_mapping = substr($this->requestMapping[$key], 1);
                    if ($key == '_id') {
                        $match[$request_mapping] = $this->convertToObjectId($column);
                    } else {
                        $match[$request_mapping] = $this->convertValueToMongoQuery($column);
                    }
                }
            }

            return $this->DBfind($match);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function findProductSku($search) {
        try {
            $match = [];
                
            if (!empty($search['_id'])) {
                !is_array($search['_id']) ? $search['_id'] = array($search['_id']) : null;
                $mapId=[];
                foreach ($search['_id'] as $id) {
                    $mapId[] = $this->convertToObjectId($id);
                }
                $match['_id'] = ['$in'=>$mapId];
            }

            if (!empty($search['sku_code'])) {
                !is_array($search['sku_code']) ? $search['sku_code'] = array($search['sku_code']) : null;
                $match['sku_code'] = ['$in'=>$search['sku_code']];
            }

            if (!empty($search['product_code'])) {
                !is_array($search['product_code']) ? $search['product_code'] = array($search['product_code']) : null;
                $match['product_code'] = ['$in'=>$search['product_code']];
            }

            !empty($search['status'])?
                $match['status'] = $search['status']:
                null;

            $pipeline = [
                [
                    '$match'=>$match
                ],
                [
                    '$lookup'=>[
                        'from'=>'dtm_product',
                        'let'=>['product_id'=>'$product_id'],
                        'pipeline'=>[
                            [
                                '$match'=>[
                                    '$expr'=>[
                                        '$and'=>[
                                            ['$eq'=>['$_id','$$product_id']],
                                            ['$ne'=>['$deleted', 1]]
                                        ]
                                    ]
                                ]
                            ]
                        ],
                        'as'=>'product_info'
                    ]
                ],
                [
                    '$lookup'=>[
                        'from'=>'dtm_evoucher',
                        'let'=>['sku_id'=>'$_id'],
                        'pipeline'=>[
                            [
                                '$match'=>[
                                    '$expr'=>[
                                        '$and'=>[
                                            ['$eq'=>['$sku_id','$$sku_id']],
                                            ['$eq'=>['$qty_in',1]],
                                            ['$eq'=>['$qty_out',0]],
                                            ['$eq'=>['$status','ACTIVE']],
                                            ['$gte'=>[
                                                    ['$dateToString'=>[
                                                        'format'=> '%Y-%m-%d',
                                                        'date'=> '$expiry_date',
                                                        'timezone' => '+07:00'
                                                    ]],
                                                    date('Y-m-d')
                                                    ]
                                            ],
                                            ['$ne'=>['$deleted', 1]]
                                        ]
                                    ]
                                ]
                            ],
                            [
                                '$group'=>[
                                    '_id'=>null,
                                    'total_qty'=>['$sum'=>1]
                                ]
                            ],
                        ],
                        'as'=>'evoucher'
                    ]
                ],
                [
                    '$addFields'=>[
                        'qty'=>[
                            '$cond'=>[
                                'if'=>['$eq'=>[
                                    ['$arrayElemAt'=>['$product_info.type',0]],
                                    'e-voucher'
                                    ]
                                ],
                                'then'=>[
                                    '$cond'=>[
                                        'if'=>['$gt'=>[['$size'=>'$evoucher'],0]],
                                        'then'=>['$arrayElemAt'=>['$evoucher.total_qty',0]],
                                        'else'=>0
                                    ]
                                ],
                                'else'=>'$qty'
                            ]
                        ],
                    ]
                ]
            ];
            return $this->DBaggregate($pipeline);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function insert($bodyData) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            return $this->DBinsert($bodyData);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function update($filter, $bodyData) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            if (!empty($filter['_id'])) {
                $filter['_id'] = $this->convertToObjectId($filter['_id']);
            }
            return $this->DBupdate($filter, $bodyData);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function findDetailProductSku($skuCode){
        try{
            $this->requestColumns .= ',product_detail';
            $this->requestMapping['product_detail'] = '$product_detail';

            $match = [
                'sku_code' => $skuCode
            ];

            $pipeline = [
                [
                    '$match'=>$match
                ],
                [
                    '$lookup'=>[
                        'from'=>'dtm_product',
                        'let'=>['product_id'=>'$product_id'],
                        'pipeline'=>[
                            [
                                '$match'=>[
                                    '$expr'=>[
                                        '$eq'=>['$_id','$$product_id']
                                        ]
                                ]
                            ]
                        ],
                        'as' => 'product_detail',
                    ]
                ],
                [
                    '$unwind'=>'$product_detail'
                ]
            ];
            return $this->DBaggregate($pipeline);
        }catch(\exception $e){
            return $this->sendError($e->getMessage(), 500, $e);
        }
    }
}