<?php
namespace App\RetailerProgram\Models;

use Infrastructure\MongoModel;

class EvoucherModel extends MongoModel {
    public $table;
    public $dbName;
    public $collectionName = "dtm_evoucher";

    public $requestColumns = "_id,product_id,product_code,sku_id,sku_code,voucher_code,expiry_date,process_number,process_id,qty_in,qty_out,status,owner_id,reference_no,barcode,qr_code,redeem_url,redeem_date,created_date,updated_date,deleted_date,count";
    public $requestMapping = [
        '_id' => '$_id',
        'product_id'=>'$product_id',
        'product_code'=>'$product_code',
        'sku_id'=>'$sku_id',
        'sku_code'=>'$sku_code',
        'voucher_code'=>'$voucher_code',
        'expiry_date'=>'$expiry_date',
        'process_number'=>'$process_number',
        'process_id'=>'$process_id',
        'qty_in'=>'$qty_in',
        'qty_out'=>'$qty_out',
        'status'=>'$status',
        'remarks'=>'$remarks',
        'owner_id'=>'$owner_id',
        'reference_no'=>'$reference_no',
        'barcode'=>'$barcode',
        'qr_code'=>'$qr_code',
        'redeem_url'=>'$redeem_url',
        'redeem_date'=>'$redeem_date',
        'created_date'=>'$created_date',
        'updated_date'=>'$updated_date',
        'deleted_date'=>'$deleted_date',
    ];

    function findAllReport($request, $orderBy = ['_id'=>-1], $pageNo = 1, $limitPerPage = 40, $requestedColumns = []) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            $limit = $limitPerPage ? $limitPerPage : false;
            $pageNo = $pageNo ? $pageNo : 1;

            if ($limit) {
                $options = $this->parsePageToSkip($pageNo, $limit);
                $skip = $options['skip'];
            }
            $columnMapping = $this->columnsMapping($requestedColumns);

            $requestMatch = [];
            foreach ($request as $key => $column) {
                if (isset($this->requestMapping[$key])) {
                    if ($key == '_id'){
                        $requestMatch['_id'] = $this->convertToObjectId($column);
                    } else {
                        $request_mapping = substr($this->requestMapping[$key], 1);
                        $requestMatch[$request_mapping] = $this->convertValueToMongoQuery($column);
                    }
                } elseif ($key == '_id' && $column == 0) $requestMatch[$key] = 0;
            }

            $sliceLen = 1;
            $pipeline = [];
            if ($request['get_detail']) {
                $pipeline = [
                    [
                        '$lookup'=>[
                            'from'=>'dtm_member',
                            'let'=>['user_id'=>'$owner_id'],
                            'pipeline'=>[
                                [
                                    '$match'=>[
                                        '$expr'=>[
                                            '$and'=>[
                                                ['$eq'=>['$_id','$$user_id']],
                                                ['$ne'=>['$deleted', 1]]
                                            ]
                                        ]
                                    ]
                                ],
                                ['$project'=>['_id'=>0]]
                            ],
                            'as'=>'member'
                        ]
                    ],
                    [
                        '$lookup'=>[
                            'from'=>'dth_order_invoice',
                            'let'=>['reference_no'=>'$reference_no'],
                            'pipeline'=>[
                                [
                                    '$match'=>[
                                        '$expr'=>[
                                            '$and'=>[
                                                ['$eq'=>['$order_id','$$reference_no']],
                                                ['$eq'=>['$status', 'COMPLETED']],
                                                ['$ne'=>['$deleted', 1]]
                                            ]
                                        ]
                                    ]
                                ],
                                ['$project'=>['_id'=>0]]
                            ],
                            'as'=>'invoice'
                        ]
                    ],
                    [
                        '$addFields'=>[
                            'full_name'=>['$arrayElemAt'=>['$member.full_name', 0]],
                            'username'=>['$arrayElemAt'=>['$member.username', 0]],
                            'cell_phone'=>['$arrayElemAt'=>['$member.cell_phone', 0]],
                            'nama_pemilik'=>['$arrayElemAt'=>['$member.input_form_data.nama_pemilik', 0]],
                            'shipping_info'=>['$arrayElemAt'=>['$invoice.shipping_info', 0]],
                        ]
                    ],
                    [
                        '$addFields'=>[
                            'nama_penerima'=>['$arrayElemAt'=>['$shipping_info.receiver_name', -1]],
                        ]
                    ],
                    [
                        '$unset'=>['_id','product_id','member','invoice','owner_id']
                    ]
                ];
                $sliceLen = 6;
            }
            if (!empty($requestMatch)) {
                $pipeline[] = ['$match' => $requestMatch];
            }
            $pipeline[] = ['$project' => $columnMapping];

            $pipeline[] = empty($orderBy) ? ['$sort' => ['_id' => -1]] : ['$sort' => $orderBy];

            if ($limit) {
                $pipeline[] = ['$skip' => $skip];
                $pipeline[] = ['$limit' => $limit];
            }

            $return = $this->DBaggregate($pipeline);
            $pipeline = array_slice($pipeline, 0, $sliceLen);

            $countPipeline = $pipeline;
            $countPipeline[] = [
                '$group' => [
                    '_id' => null,
                    'count' => ['$sum' => 1],
                ],
            ];

            $totAggDB = $this->DBaggregate($countPipeline);
            $total = $totAggDB['result'][0]->count;

            if ($limit) {
                $total_page = ceil(intval($total) / $limit);
            } else {
                $total_page = 1;
            }

            $result = [
                'values' => $return['result'],
                    'total_all_values' => $total,
                    'total_page' => $total_page,
                    'skip' => $skip,
            ];

            return $this->sendResult($result);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function findReportSummary($request, $orderBy = ['_id'=>-1], $pageNo = 1, $limitPerPage = 40, $requestedColumns = []) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            $limit = $limitPerPage ? $limitPerPage : false;
            $pageNo = $pageNo ? $pageNo : 1;

            if ($limit) {
                $options = $this->parsePageToSkip($pageNo, $limit);
                $skip = $options['skip'];
            }
            $columnMapping = $this->columnsMapping($requestedColumns);

            $requestMatch = [];
            foreach ($request as $key => $column) {
                if (isset($this->requestMapping[$key])) {
                    if ($key == '_id'){
                        $requestMatch['_id'] = $this->convertToObjectId($column);
                    } else {
                        $request_mapping = substr($this->requestMapping[$key], 1);
                        $requestMatch[$request_mapping] = $this->convertValueToMongoQuery($column);
                    }
                } elseif ($key == '_id' && $column == 0) $requestMatch[$key] = 0;
            }

            $pipeline = [];
            if (!empty($requestMatch)) {
                $pipeline[] = ['$match' => $requestMatch];
            }

            $pipeline = array_merge(
                $pipeline,
                [
                    [
                        '$addFields'=>[
                            'available'=>[
                                '$cond'=>[
                                    'if'=>[
                                        '$and'=>[
                                            ['$eq'=>['$qty_in',1]],
                                            ['$eq'=>['$qty_out',0]],
                                            ['$eq'=>['$status','ACTIVE']],
                                            ['$gte'=>[
                                                '$expiry_date', 
                                                $this->convertToMongoDateTime(date('Y-m-d'))
                                            ]]
                                        ]
                                    ],
                                    'then'=>1,
                                    'else'=>0
                                ]
                            ],
                            'sold'=>[
                                '$cond'=>[
                                    'if'=>[
                                        '$and'=>[
                                            ['$eq'=>['$qty_in',1]],
                                            ['$eq'=>['$qty_out',1]],
                                            ['$eq'=>['$status','RESERVED']],
                                        ]
                                    ],
                                    'then'=>1,
                                    'else'=>0
                                ]
                            ],
                            'used'=>[
                                '$cond'=>[
                                    'if'=>[
                                        '$eq'=>['$status','REDEEM']
                                    ],
                                    'then'=>1,
                                    'else'=>0
                                ]
                            ],
                            'inactive'=>[
                                '$cond'=>[
                                    'if'=>[
                                        '$and'=>[
                                            ['$eq'=>['$qty_in',1]],
                                            ['$eq'=>['$qty_out',0]],
                                            ['$eq'=>['$status','INACTIVE']],
                                        ]
                                    ],
                                    'then'=>1,
                                    'else'=>0
                                ]
                            ],
                            'expired'=>[
                                '$cond'=>[
                                    'if'=>[
                                        '$and'=>[
                                            ['$eq'=>['$qty_in',1]],
                                            ['$eq'=>['$qty_out',0]],
                                            ['$eq'=>['$status','ACTIVE']],
                                            ['$lt'=>[
                                                '$expiry_date', 
                                                $this->convertToMongoDateTime(date('Y-m-d'))
                                            ]]
                                        ]
                                    ],
                                    'then'=>1,
                                    'else'=>0
                                ]
                            ]
                        ]
                    ],
                    [
                        '$group'=>[
                            '_id'=>['expiry_date'=>'$expiry_date','product_id'=>'$product_id'],
                            'expiry_date'=>['$first'=>'$expiry_date'],
                            'product_code'=>['$first'=>'$product_code'],
                            'qty_available'=>['$sum'=>'$available'],
                            'qty_sold'=>['$sum'=>'$sold'],
                            'qty_used'=>['$sum'=>'$used'],
                            'qty_inactive'=>['$sum'=>'$inactive'],
                            'qty_expired'=>['$sum'=>'$expired']
                        ]
                    ],
                    [
                        '$lookup'=>[
                            'from'=>'dtm_product',
                            'let'=>['product_id'=>'$_id.product_id'],
                            'pipeline'=>[
                                [
                                    '$match'=>[
                                        '$expr'=>[
                                            '$and'=>[
                                                ['$eq'=>['$_id','$$product_id']],
                                                ['$ne'=>['$deleted', 1]]
                                            ]
                                        ]
                                    ]
                                ],
                                ['$project'=>['_id'=>0]]
                            ],
                            'as'=>'product'
                        ]
                    ],
                    [
                        '$addFields'=>[
                            'product_name'=>['$arrayElemAt'=>['$product.product_name',0]],
                            'redeem_type'=>['$arrayElemAt'=>['$product.redeem_type',0]],
                            'product'=>['$arrayElemAt'=>['$product',0]],
                        ]
                    ]
                ]
            );
            $sliceLen = 5;

            print_r($columnMapping);
            $pipeline[] = ['$project' => $columnMapping];
            $pipeline[] = empty($orderBy) ? ['$sort' => ['expiry_date' => -1]] : ['$sort' => $orderBy];

            if ($limit) {
                $pipeline[] = ['$skip' => $skip];
                $pipeline[] = ['$limit' => $limit];
            }

            $return = $this->DBaggregate($pipeline);
            $pipeline = array_slice($pipeline, 0, $sliceLen);
            $pipeline[0] = [
                '$group' => [
                    '_id' => null,
                    'count' => ['$sum' => 1],
                ],
            ];

            $totAggDB = $this->DBaggregate($pipeline);
            $total = $totAggDB['result'][0]->count;

            if ($limit) {
                $total_page = ceil(intval($total) / $limit);
            } else {
                $total_page = 1;
            }

            $result = [
                'values' => $return['result'],
                    'total_all_values' => $total,
                    'total_page' => $total_page,
                    'skip' => $skip,
            ];

            return $this->sendResult($result);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function find($filter) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            $match = [];
            foreach ($filter as $key => $column) {
                if (isset($this->requestMapping[$key])) {
                    $request_mapping = substr($this->requestMapping[$key], 1);
                    if ($key == '_id') {
                        $match[$request_mapping] = $this->convertToObjectId($column);
                    } else {
                        $match[$request_mapping] = $this->convertValueToMongoQuery($column);
                    }
                }
            }

            return $this->DBfind($match);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function findEvoucher($filter = [], $limit = false, $sortBy = ['_id'=>-1]) {
        try {
            $this->requestColumns .= ',product_info';
            $this->requestMapping['product_info'] = '$product_info';

            $match = [];
            if (!empty($filter['voucher_code'])) {
                is_string($filter['voucher_code']) ? $filter['voucher_code'] = [$filter['voucher_code']] : null;
                $match['voucher_code'] = ['$in'=>$filter['voucher_code']];
            }

            if (!empty($filter['process_id'])) {
                is_string($filter['process_id']) ? $filter['process_id'] = [$filter['process_id']] : null;
                $match['process_id'] = ['$in'=>$filter['process_id']];
            }

            if (!empty($filter['process_number'])) {
                is_string($filter['process_number']) ? $filter['process_number'] = [$filter['process_number']] : null;
                $match['process_number'] = ['$in'=>$filter['process_number']];
            }

            !empty($filter['status'])?
                $match['status'] = $filter['status']:
                null;

            if (!empty($filter['owner_id'])) {
                $match['owner_id'] = $this->convertToObjectId($filter['owner_id']);
            }

            if (!empty($filter['product_id'])){
                is_string($filter['product_id']) ? $filter['product_id'] = [$filter['product_id']] : null;
                foreach ($filter['product_id'] as $key => $value) {
                    $filter['product_id'][$key] = $this->convertToObjectId($value);
                }
                $match['product_id'] = ['$in'=>$filter['product_id']];
            }

            if (!empty($filter['sku_id'])){
                is_string($filter['sku_id']) ? $filter['sku_id'] = [$filter['sku_id']] : null;
                foreach ($filter['sku_id'] as $key => $value) {
                    $filter['sku_id'][$key] = $this->convertToObjectId($value);
                }
                $match['sku_id'] = ['$in'=>$filter['sku_id']];
            }

            if (!empty($filter['product_code'])){
                is_string($filter['product_code']) ? $filter['product_code'] = [$filter['product_code']] : null;
                $match['product_code'] = ['$in'=>$filter['product_code']];
            }

            if (!empty($filter['sku_code'])){
                is_string($filter['sku_code']) ? $filter['sku_code'] = [$filter['sku_code']] : null;
                $match['sku_code'] = ['$in'=>$filter['sku_code']];
            }

            if (!empty($filter['expiry_date'])){

                if (is_string($filter['expiry_date'])) {
                    $match['expiry_date'] = $this->convertToMongoDateTime($filter['expiry_date']);
                } else {
                    if (!empty($filter['expiry_date']['from'])) {
                        $match['expiry_date']['$gte'] = $this->convertToMongoDateTime($filter['expiry_date']['from']);
                    }
                    if (!empty($filter['expiry_date']['to'])) {
                        $match['expiry_date']['$lte'] = $this->convertToMongoDateTime($filter['expiry_date']['to']);
                    }
                }
            }

            if (!empty($filter['reference_no'])) {
                $match['reference_no'] = $filter['reference_no'];
            }

            !empty($filter['qty_in']) ? $match['qty_in'] = $filter['qty_in'] : null;
            !empty($filter['qty_out']) ? $match['qty_out'] = $filter['qty_out'] : null;
            
            $pipeline = [
                [
                    '$match'=>$match
                ],
                [
                    '$lookup'=>[
                        'from'=>'dtm_product_sku',
                        'let'=>['sku_id'=>'$sku_id'],
                        'pipeline'=>[
                            [
                                '$match'=>[
                                    '$expr'=>[
                                        '$and'=>[
                                            ['$eq'=>['$_id','$$sku_id']],
                                            ['$ne'=>['$deleted', 1]]
                                        ]
                                    ]
                                ]
                            ],
                            [
                                '$lookup'=>[
                                    'from'=>'dtm_product',
                                    'let'=>['product_id'=>'$product_id'],
                                    'pipeline'=>[
                                        [
                                            '$match'=>[
                                                '$expr'=>[
                                                    '$and'=>[
                                                        ['$eq'=>['$_id','$$product_id']],
                                                        ['$ne'=>['$deleted', 1]]
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ],
                                    'as'=>'product'
                                ]
                            ],
                            [
                                '$replaceRoot'=>[
                                    'newRoot'=>[
                                        '$mergeObjects'=>[
                                            ['$arrayElemAt'=>['$product', 0]],
                                            '$$ROOT'
                                        ]
                                    ]
                                ]
                            ],
                            [
                                '$unset'=>['_id','product_id','product']
                            ]
                        ],
                        'as'=>'product_info'
                    ]
                ],
                [
                    '$addFields'=>[
                        'product_info'=>['$arrayElemAt'=>['$product_info', 0]]
                    ]
                ],
                [
                    '$project'=>$this->requestMapping
                ]
                // [
                //     '$unset'=>['_id','product_id','sku_id','owner_id']
                // ]
            ];

            return $this->DBaggregate($pipeline);

        } catch (\Exception $e) {
            return $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function findEvoucherLists($filter, $option=[]) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            $this->requestColumns .= ',product_info';
            $this->requestMapping['product_info'] = '$product_info';

            switch ($option['option']) {
                case 'available':
                    $filter['qty_in'] = 1;
                    $filter['qty_out'] = 1;
                    $filter['status'] = 'RESERVED';
                    $filter['expiry_date'] = ['$gte'=>$this->convertToMongoDateTime(date('Y-m-d H:i:s'))];
                    break;
                
                case 'used':
                    $filter['qty_in'] = 1;
                    $filter['qty_out'] = 1;
                    $filter['status'] = 'REDEEM';
                    break;

                case 'expired':
                    $filter['qty_in'] = 1;
                    $filter['qty_out'] = 1;
                    $filter['status'] = 'RESERVED';
                    $filter['expiry_date'] = ['$lt'=>$this->convertToMongoDateTime(date('Y-m-d H:i:s'))];
                    break;
            }
            
            $pipeline = [
                [
                    '$match'=>$filter
                ],
                [
                    '$lookup'=>[
                        'from'=>'dtm_product_sku',
                        'let'=>['sku_id'=>'$sku_id'],
                        'pipeline'=>[
                            [
                                '$match'=>[
                                    '$expr'=>[
                                        '$and'=>[
                                            ['$eq'=>['$_id','$$sku_id']],
                                            ['$ne'=>['$deleted', 1]]
                                        ]
                                    ]
                                ]
                            ],
                            [
                                '$lookup'=>[
                                    'from'=>'dtm_product',
                                    'let'=>['product_id'=>'$product_id'],
                                    'pipeline'=>[
                                        [
                                            '$match'=>[
                                                '$expr'=>[
                                                    '$and'=>[
                                                        ['$eq'=>['$_id','$$product_id']],
                                                        ['$ne'=>['$deleted', 1]]
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ],
                                    'as'=>'product'
                                ]
                            ],
                            [
                                '$replaceRoot'=>[
                                    'newRoot'=>[
                                        '$mergeObjects'=>[
                                            ['$arrayElemAt'=>['$product', 0]],
                                            '$$ROOT'
                                        ]
                                    ]
                                ]
                            ],
                            [
                                '$unset'=>['_id','product_id','product']
                            ]
                        ],
                        'as'=>'product_info'
                    ]
                ],
                [
                    '$addFields'=>[
                        'product_info'=>['$arrayElemAt'=>['$product_info', 0]]
                    ]
                ],
                [
                    '$unset'=>['_id','product_id','sku_id','owner_id','voucher_code','qr_code','barcode','redeem_url']
                ]
            ];

            return $this->DBaggregate($pipeline);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function getAvailableStock($productCode, $skuCode, $limit) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            $filter = [
                'product_code'=>$productCode,
                'sku_code'=>$skuCode,
                'expiry_date'=>['$gte'=>$this->convertToMongoDateTime(date('Y-m-d'))],
                'status'=>'ACTIVE',
                'qty_in'=>1,
                'qty_out'=>0
            ];

            if (!empty($limit)) {
                $opt = ['limit'=>$limit];
            }
            $opt['sort'] = ['expiry_date'=>1];
            return $this->DBfind($filter, $opt);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage(), 500);
        }
    }

    function insert($bodyData) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            return $this->DBinsert($bodyData);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function update($filter, $bodyData) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            return $this->DBupdate($filter, $bodyData);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }
}