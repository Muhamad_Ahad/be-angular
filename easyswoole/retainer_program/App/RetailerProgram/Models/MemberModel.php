<?php
namespace App\RetailerProgram\Models;

use Infrastructure\MongoModel;

class MemberModel extends MongoModel {
    public $table;
    public $dbName;
    public $collectionName = "dtm_member";

    public $requestColumns = "_id,full_name,username,password,cell_phone,dob,gender,member_address,status,created_date,updated_date,deleted_date,input_form_data,additional_info,point_balance,count";
    public $requestMapping = [
        '_id' => '$_id',
        'full_name'=>'$full_name',
        'username'=>'$username',
        'password'=>'$password',
        'cell_phone'=>'$cell_phone',
        'dob'=>'$dob',
        'gender'=>'$gender',
        'member_address'=>'$member_address',
        'status'=>'$status',
        'input_form_data'=>'$input_form_data',
        'additional_info'=>'$additional_info',
        'point_balance'=>'$point_balance',
        'created_date'=>'$created_date',
        'updated_date'=>'$updated_date',
        'deleted_date'=>'$deleted_date',
    ];

    function findAllReport($request, $orderBy = ['_id'=>-1], $pageNo = 1, $limitPerPage = 40, $requestedColumns = []) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            $limit = $limitPerPage ? $limitPerPage : false;
            $pageNo = $pageNo ? $pageNo : 1;

            if ($limit) {
                $options = $this->parsePageToSkip($pageNo, $limit);
                $skip = $options['skip'];
            }
            $columnMapping = $this->columnsMapping($requestedColumns);

            $requestMatch = [];
            foreach ($request as $key => $column) {
                if (isset($this->requestMapping[$key])) {
                    if ($key == '_id'){
                        $requestMatch['_id'] = $this->convertToObjectId($column);
                    } else {
                        $request_mapping = substr($this->requestMapping[$key], 1);
                        $requestMatch[$request_mapping] = $this->convertValueToMongoQuery($column);
                    }
                } elseif ($key == '_id' && $column == 0) $requestMatch[$key] = 0;
                else if (strpos($key, '.') !== false) {
                    $keyName = explode('.', $key);
                    if (!empty($this->requestMapping[$keyName[0]])) {
                        $requestMatch[$key] = $this->convertValueToMongoQuery($column);
                    }
                }
            }

            $pipeline = [
                [
                    '$lookup'=>[
                        'from'=>'dtm_points_inventory',
                        'let'=>['user_id'=>'$_id'],
                        'pipeline'=>[
                            [
                                '$match'=>[
                                    '$expr'=>[
                                        '$and'=>[
                                            ['$eq'=>['$user_id','$$user_id']],
                                            ['$gte'=>[
                                                ['$dateToString'=>[
                                                    'format'=> '%Y-%m-%d',
                                                    'date'=> '$expiry_date',
                                                    'timezone' => '+07:00'
                                                ]],
                                                date('Y-m-d')
                                                ]
                                            ],
                                            ['$eq'=>['$status','ACTIVE']],
                                            ['$gt'=>['$points', 0]],
                                            ['$ne'=>['$deleted', 1]]
                                        ]
                                    ]
                                ]
                            ],
                            [
                                '$group'=>[
                                    '_id'=>null,
                                    'points'=>['$sum'=>'$points']
                                ]
                            ]
                        ],
                        'as'=>'points'
                    ]
                ],
                [
                    '$addFields'=>[
                        'point_balance'=>['$arrayElemAt'=>['$points.points', 0]]
                    ]
                ]
            ];
            $sliceLen = 3;

            if (!empty($requestMatch)) {
                $pipeline[] = ['$match' => $requestMatch];
            }
            $pipeline[] = ['$project' => $columnMapping];

            $pipeline[] = empty($orderBy) ? ['$sort' => ['_id' => -1]] : ['$sort' => $orderBy];

            if ($limit) {
                $pipeline[] = ['$skip' => $skip];
                $pipeline[] = ['$limit' => $limit];
            }

            $return = $this->DBaggregate($pipeline);
            $pipeline = array_slice($pipeline, 0, $sliceLen);

            $pipeline[] = [
                '$group' => [
                    '_id' => null,
                    'count' => ['$sum' => 1],
                ],
            ];

            $totAggDB = $this->DBaggregate($pipeline);
            $total = $totAggDB['result'][0]->count;

            if ($limit) {
                $total_page = ceil(intval($total) / $limit);
            } else {
                $total_page = 1;
            }

            $result = [
                'values' => $return['result'],
                    'total_all_values' => $total,
                    'total_page' => $total_page,
                    'skip' => $skip,
            ];

            return $this->sendResult($result);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function find($filter) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            $match = [];
            foreach ($filter as $key => $column) {
                if (isset($this->requestMapping[$key])) {
                    $request_mapping = substr($this->requestMapping[$key], 1);
                    if ($key == '_id') {
                        $match[$request_mapping] = $this->convertToObjectId($column);
                    } else {
                        $match[$request_mapping] = $this->convertValueToMongoQuery($column);
                    }
                }
            }

            return $this->DBfind($match);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function findByUsernameOrId($usernameOrId, $findActive = null) {
        try {
            $usernameOrId = $this->convertToObjectId($usernameOrId);
            $match = [
                '$or'=>[
                    ['_id'=>$usernameOrId],
                    ['username'=>$usernameOrId]
                ]
            ];

            !empty($findActive)?
                $match['status'] = $findActive:
                null;

            return $this->DBfind($match);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function insert($bodyData) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            return $this->DBinsert($bodyData);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    function update($filter, $bodyData) {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            if (!empty($filter['_id'])) {
                $filter['_id'] = $this->convertToObjectId($filter['_id']);
            }
            return $this->DBupdate($filter, $bodyData);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }
}