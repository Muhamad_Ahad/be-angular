<?php
namespace App\RetailerProgram\Models;

use Infrastructure\MongoModel;
use PhpOffice\PhpSpreadsheet\Calculation\MathTrig\Arabic;

class CategoryModel extends MongoModel
{
    public $table;
    public $dbName;
    public $collectionName = "dtm_category";
    public $requestColumns = "_id,name,code,imageUrl,type,key,count";

    public $requestMapping = [
        '_id' => '$_id',
        'name' => '$name',
        'code' => '$code',
        'imageUrl' => '$imageUrl',
        'type' => '$type',
        'key' => '$key'
    ];

    public function findAllReport($request, $orderBy = ['_id'=>-1], $pageNo = 1, $limitPerPage, $requestedColumns = [])
    {
        // tambahkan di $requestColumns-> 'count' supaya paginationnya berfungsi
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            $limit = $limitPerPage ? $limitPerPage : false;
            $pageNo = $pageNo ? $pageNo : 1;
            
            if ($limit) {
                $options = $this->parsePageToSkip($pageNo, $limit);
                $skip = $options['skip'];
            }
            $columnMapping = $this->columnsMapping($requestedColumns);
            $requestMatch = [];
            foreach ($request as $key=>$column) {
                if (isset($this->requestMapping[$key])) {
                    if ($key == '_id'){
                        $requestMatch['_id'] = $this->convertToObjectId($column);
                    } else {
                        $request_mapping = substr($this->requestMapping[$key], 1);
                        $requestMatch[$request_mapping] = $this->convertValueToMongoQuery($column);
                    }
                } elseif ($key == '_id' && $column == 0) $requestMatch[$key] = 0;
            }
            
            $pipeline = [];
            $pipeline[] = ['$project' => $columnMapping];
            if (!empty($requestMatch)) {
                $pipeline[] = ['$match' => $requestMatch];
            }
            $pipeline[] = empty($orderBy) ? ['$sort' => ['_id' => -1]] : ['$sort' => $orderBy];
            
            if ($limit) {
                $pipeline[] = ['$skip' => $skip];
                $pipeline[] = ['$limit' => $limit];
            }

            $return = $this->DBaggregate($pipeline);
            unset($pipeline[2]);
            unset($pipeline[3]);
            unset($pipeline[4]);

            $countPipeline = $pipeline;
            $countPipeline[2] = [
                '$group' => [
                    '_id'=> null,
                    'count' => [ '$sum' => 1]
                ]
            ];
            
            $totAggDB = $this->DBaggregate($countPipeline);
            $total = $totAggDB['result'][0]->count;
            
            if ($limit) {
                $total_page = ceil(intval($total) / $limit);
            } else {
                $total_page = 1;
            }

            $result = [
                'values'=>
                    $return['result'],
                    'total_all_values' => $total,
                    'total_page'=> $total_page,
                    'skip' => $skip,
            ];
                    
            return $this->sendResult($result);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    public function findCategory($filter = [])
    {
        if ($this->error !== false) {
            return $this->sendResult();
        }
        try {
            !empty($filter['_id']) ? 
                $filter['_id'] = $this->convertToObjectId($filter['_id']): 
                null;

            return $this->DBfind($filter);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    public function getcategoryList($search = [], $groupType = true)
    {
        if ($this->error !== false) {
            return $this->sendResult();
        }
        
        try {
            $this->requestColumns .= ",category_list";

            $pipeline = [];
            $match = [];
            if (!empty($search)) {
                foreach ($search as $key => $value) {
                    !is_array($value) ? $value = array($value) : null;
                    $match[$key] = ['$in'=>$value];
                }
            }
            
            !empty($match) ? $pipeline[] = ['$match'=>$match]: null;

            $pipeline[] = [
                '$sort'=>[
                    '_id'=>-1
                ]
            ];

            ($groupType == true) ?
                $pipeline[] = [
                    '$group'=>[
                        '_id'=>'$type',
                        'type'=>['$first'=>'$type'],
                        'category_list'=>[
                            '$push'=>'$$ROOT'
                        ]
                    ]
                ]:
                null;

            $pipeline[] = ['$project'=>['_id'=>0]];


            return $this->DBaggregate($pipeline);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }
    
    public function findByCategoryCodeOrName($param, $additionalFilter = [])
    {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            if(is_array($param)) {
                $filter = ['$or'=>[
                            ['name'=>['$in'=>$param]],
                            ['code'=>['$in'=>$param]],
                            ['key'=>['$in'=>$param]],
                        ]
                ];
            } else {
                $filter = ['$or'=>[
                                    ['name'=>$param],
                                    ['code'=>$param],
                                    ['key'=>$param]
                                ]
                        ];
            }

            if(!empty($additionalFilter['type'])) {
                if(!is_array($additionalFilter['type'])) $additionalFilter['type'] = array($additionalFilter['type']);
                $filter['type'] = ['$in'=>$additionalFilter['type']];
            }
            return $this->DBfind($filter);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    public function removeCategory($filter)
    {
        if ($this->error !== false) {
            return $this->sendResult();
        }
        try {

            return $this->DBdelete($filter);

        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }        
    }

    public function update($filter, $getBody)
    {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {

            return $this->DBupdate($filter, $getBody);

        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }

    public function insert($getBody)
    {
        if ($this->error !== false) {
            return $this->sendResult();
        }

        try {
            return $this->DBinsert($getBody);
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 500, $e);
        }
    }
}
